Quel est le besoin en cartes des itinéraires pour le DNF? 
---------------------------------------------------------

**Mail de Marie Wenin du 15/7/2020:**

Bonjour à tous les deux,


Voici les premières réactions reçues suite à ma demande en interne quant aux « exigences » de format pour les itinéraires transmis au DNF

Évidemment, d’un cantonnement à l’autre ils ne travaillent pas de la même manière, ca aurait été trop simple. Ceci dit je pense qu’il y a moyen de trouver une solution qui convienne à tous

Je continue à vous faire suivre si je reçois d’autres réactions

On peut en discuter lors de notre prochaine réunion (de vendredi)


Marie

 

# REPONSE 1 ______________________________________________________________

 

Il faut se référer à l’AGW du 27 mai 2009 ou le modifier…

 

Jusqu’à présent il faut une carte ou extrait de carte qui indique le tracé. L’intérêt du fond IGN c’est qu’il renseigne les sentiers et chemins officiels ce qui n’est pas toujours le cas avec d’autres supports (GPX)

 

# REPONSE 2 ______________________________________________________________

 

De nombreuses demandes utilisent déjà des sites style openrunner. On reçoit alors un mail avec soit le lien (ex. https://www.openrunner.com/r/9281813) soit le fichier GPX. Le gros avantage est de pouvoir zoomer sur le carte : les cartes papier que l’on reçoit avec certaines demandes ne sont parfois pas très lisibles, collages de plusieurs A4 impossible à scanner/copier... L’inconvénient est que la plupart des agents ne savent pas ouvrir un GPX ce qui nécessite que le cartographe du cantonnement produise une carte PDF à partir du GPX envoyé d’où ma préférence pour un lien plutôt qu’un GPX.

 

Une autre piste serait d’avoir une base de données reprenant une couche vectorielle type IGN/OpenStreetMap (en fonction de l’endroit les données sont plus à jour/correcte sur l’un ou l’autre). Le demandeur y sélectionnerait les segments qui composent le circuit. Il pourrait toujours digitaliser au cas où le segment souhaité est inexistant. L’avantage est que l’on pourrait ajouter d’autres informations sur les segments existants (ex. autorisation d’accès en fonction du type d’usager, fermetures diverses…). Cela permettrait de fournir une base de travail aux organisateurs d’événements et simplifierait le travail (ex. le circuit créé ne comprend que des segments autorisés pour le type d’usager en question donc « simple » notification). Mais le chantier est tout autre…

 

# REPONSE 3 ______________________________________________________________
 

Le cantonnement de Malmedy demande systématiquement le fichier GPX et n’exige plus de cartes IGN.

 

Nous visualisons la trace sur WALONMAP et la partageons avec les agents concernés.

 

Nous n’imprimons aucune carte.

 