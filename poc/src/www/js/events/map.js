import { transform } from 'ol/proj';

import { updateRoutingFormWithCoordinates, doRouting } from "../routing/routing";
import { addWayPoint, getWayPoints } from '../map/layers';
import { uuidv4 } from "../utils/uid";
import { addModifyInteraction, onMarkerChange, onItineraryChange } from '../map/interactions';
import { addIgnOverlayToOrtho } from '../map/base-layers';

export const mapEvents = (map) => {
    map.on('singleclick', (e) => {
        
        const uid = uuidv4();
        addWayPoint(e.coordinate, uid);
        const coordinatesWGS84 = transform(e.coordinate, 'EPSG:3857', 'EPSG:4326');
        updateRoutingFormWithCoordinates(coordinatesWGS84, uid);

        const wayPoints = getWayPoints();
        if (wayPoints.length >= 2) {
            doRouting(wayPoints);
        }
    });

    addModifyInteraction(map);
    onMarkerChange();
    onItineraryChange();
    addIgnOverlayToOrtho(map);
}



 
// TODO: event for moving the markers

// TODO: event for deleting the markers

// TODO: event for moving the track (add marker on click on track?)