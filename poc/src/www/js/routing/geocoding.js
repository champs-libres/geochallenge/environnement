import { Geocode } from 'openrouteservice-js/npm_distribution';

import { transform } from 'ol/proj';

import { doRouting } from './routing'; 
import { addWayPoint, getWayPoints } from '../map/layers';
import { uuidv4 } from '../utils/uid';

const apiKey = '5b3ce3597851110001cf6248d35391925fe04b8792859894153cde3c'; // TODO

export let geocodePoints = [];

const updateGeocodeInputs = () => {
    const geocodeInputs = document.querySelectorAll('form.geocode > div > input');
    for (let i = 0; i < geocodePoints.length; i++) {
        if (geocodePoints[i].hasOwnProperty('properties')){
            if (geocodePoints[i].properties.label !== undefined) {
                geocodeInputs[i].value = geocodePoints[i].properties.label;
            }
        }
    }
};

const onSelectGeocodeResult = (feature) => {
    removeGeocodeResponse();
    geocodePoints.push(feature);
    updateGeocodeInputs();
    console.log(feature)
    const uid = uuidv4();
    addWayPoint(transform(feature.geometry.coordinates, 'EPSG:4326', 'EPSG:3857'), uid);
    //addUidToInput(uid); //TODO
    const wayPoints = getWayPoints();
    if (wayPoints.length >= 2){
        doRouting(wayPoints);
    }
};

const addGeocodeFeature = (parentNode, feature) => {
    const featureDiv = document.createElement('div');
    featureDiv.classList.add('geocode__results__item');
    const featureLabelDiv = document.createElement('div');
    const featureRegionDiv = document.createElement('div');
    featureLabelDiv.innerHTML = feature.properties.label;
    featureLabelDiv.addEventListener('click', (_e) => onSelectGeocodeResult(feature));
    featureRegionDiv.innerText = 
        `${feature.properties.region}, ${feature.properties.macroregion}, ${feature.properties.country}`;

    featureDiv.appendChild(featureLabelDiv);
    featureDiv.appendChild(featureRegionDiv);

    parentNode.appendChild(featureDiv);
};

const removeGeocodeResponse = () => {
    const oldGeocodeBox = document.querySelectorAll('div.geocode__results');
    if (oldGeocodeBox.length > 0) {
        oldGeocodeBox.forEach(e => e.parentNode.removeChild(e));
    }
}

const addGeocodeResponse = (parentNode, response) => {

    removeGeocodeResponse();

    const geocodeBox = document.createElement('div');
    geocodeBox.classList.add('geocode__results');
    response.features.forEach(f => {
        addGeocodeFeature(geocodeBox, f);
    });

    parentNode.appendChild(geocodeBox);
};


export const geocodeInput = (input, parentNode) =>  {
     /**
    * Geocode a string entered in an input box and suggest locations below the box
    * @param {string} input - the string to geocode
    * @param {string} parentNode - a DOM element to add suggestions results
    */

    const geocoder = new Geocode({
        api_key: apiKey
    });
    
    geocoder.geocode({
        text: input,
        boundary_country: ["BE"] //TODO: plutot utiliser une bbox qui englobe la belgique pour permettre des résultats hors BE mais proches
    }).then((json) => {
        addGeocodeResponse(parentNode, json);
    })
    .catch((err) => {
        let response = JSON.stringify(err, null, "\t")
        console.log(response);
        response = response.replace(/(\n)/g, '<br>');
        response = response.replace(/(\t)/g, '&nbsp;&nbsp;');
        parentNode.innerHTML = "<h3>Error</h3><p>" + response + "</p>";
        // TODO messageToast erreur de geocodage
    });
};

