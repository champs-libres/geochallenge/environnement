
\newpage

# Résumé {#sec:resume}

_orgarando_ est une application de cartographie qui facilite l'organisation de manifestations sportives. Cette application vise à permettre aux **organisateurs de manifestations sportives** de tracer leurs itinéraires et de savoir quelles **démarches administratives** sont à réaliser pour obtenir les **autorisations par rapport à leurs itinéraires**. Les deux mises en situation suivantes définissent le besoin.

<!-- \begin{mdframed}[linecolor=green!60, roundcorner=10pt, linewidth=2pt, leftmargin=-0.5cm, innerleftmargin=0.5cm, rightmargin=-0.5cm, innerrightmargin=0.5cm] -->


\setlength{\leftskip}{0.5cm}

\textbf{Jean-Pierre et sa marche Adeps}

\emph{Nouvellement élu président de son club de belote, Jean-Pierre est chargé d'organiser une marche Adeps dans l'espoir de récolter des fonds. Après avoir déterminé le tracé sur des cartes IGN au cours de longues soirées et après repérage sur le terrain, il apprend qu'il doit demander des autorisations auprès des 2 communes traversées et de le DNF afin de valider son tracé. Jean-Pierre a d'ailleurs des doutes quant à savoir si on peut traverser les terres du baron de Hérode. Lors d'un de ses déplacement à Namur, Jean-Pierre fait des photocopies A1 des cartes IGN concernées puis trace le trajet à l'aide d'un fluo. Après plusieurs allers-retours entre la commune et le DNF et de multiples annotations sur la carte (car il a fallu faire des détours pour éviter les chemins traversant la propriété du baron de Hérode, ainsi que toute une portion d'un bois qui sera justement en chasse ce jour-là), il doit refaire des photocopies à Namur afin de soumettre sa carte finale. Bien que retraité, Jean-Pierre se dit que soumettre un tracé pour un tel événement est une démarche qui prend pas mal de temps. C'est aussi l'avis de Ludovic, le stagiaire DNF qui s'est empressé d'encoder dans Google Earth le tracé reçu à trois reprises avant d'entendre à chaque fois l'ingénieure lui dire qu'il fallait le changer.}


\textbf{Sarah et son Run\&Bike}

\emph{Trailleuse chevronnée, jamais à court d'idée, Sarah décide d'organiser avec son club de running un événement sportif novateur: un Run \& Bike d'orientation en Crocs®. Adepte des nouvelles technologies, elle n'a aucun mal à planifier et créer les 7 parcours au format GPX à l'aide de l'application Strava. Par contre, elle tombe des nues lorsqu'elle apprend qu'il faut faire des demandes officielles à toutes les communes traversées (4!) et à le DNF, avec des délais variables et même des supports différents: telle commune exige une copie des cartes papier tandis que les autres communes se satisfont d'une version électronique. Deuxième déconvenue, elle prépare ses 7 cartes avec un outil trouvé en ligne sur un forum: InkAtlas, mais Manu, l'agent DNF qui reçoit les cartes, les lui renvoie car la résolution de la carte ne permet pas de voir si un des tracés passe dans la zone de quiétude de son triage. Habitué des cartes IGN, Manu préférerait d'ailleurs recevoir ces cartes avec un fond IGN. Enfin, dernière déconvenue: lors du balisage la veille de l'événement, Sarah et son équipe s'aperçoivent qu'un balisage d'une marche Adeps pour un club de belote organisée le même jour chevauche ses itinéraires sur plusieurs tronçons.}

\setlength{\leftskip}{0pt}
<!-- \end{mdframed} -->


Ces deux mises en situations illustrent le besoin que nous souhaitons combler: **une application de cartographie pour faciliter les démarches administratives liées à l'organisation de manifestations sportives**. 


De très nombreuses manifestations sportives sont organisées chaque année en Wallonie: promenades à pied, à VTT, courses à pied, etc. Ces manifestations, souvent organisées par des bénévoles, requièrent des **autorisations des communes** concernées ainsi que du **cantonnement du Département Nature et Forêt (DNF)** local, en fonction du **tracé précis** de l'événement. Il n'existe pas à ce jour de centralisation des démarches officielles et de formulaire unique en Wallonie pour **transmettre les tracés des parcours à ces administrations**. Les organisateurs sont en outre souvent dépourvus d'information quant aux zones où ce type d'événement est proscrit. Nous proposons de créer un outil web cartographique à destination des organisateurs de manifestations sportives pour créer et transmettre les tracés des parcours sous forme numérique et/ou papier. Cet outil proposera le réseau de chemins et sentiers accessibles pour organiser une randonnée, en informant sur les contraintes d'exclusion et sur les démarches à effectuer. L'organisateur pourra créer son itinéraire dans une carte interactive. Au final, l'organisateur disposera d'une **carte de son itinéraire** et une liste des **démarches administratives** à réaliser. 



