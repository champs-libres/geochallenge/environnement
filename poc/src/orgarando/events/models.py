from django.contrib.gis.db import models

# Create your models here.

class Event(models.Model):
    EVENT_TYPES = (
            ('None', 'Aucune'),
            ('ADEPS', 'ADEPS'),
            ('COURS_FORME', 'Je cours pour ma forme'),
            )
    itinerary = models.LineStringField(srid=31370)
    createdAt = models.DateField(auto_now=True)
    eventDateStart = models.DateField()
    eventDateEnd   = models.DateField()
    eventType = models.CharField(max_length=100,
            choices=EVENT_TYPES)




