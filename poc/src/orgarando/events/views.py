from django.shortcuts import render
from .models import Event
from .serializer import EventSerializer, EventProcedureSerializer
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from dnf.processor import DNFProcessor

# Create your views here.
class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    @action(detail=True, methods=['get'])
    def procedures(self, request, pk=None):
        event = self.get_object() 
        procedures = list()
        dnfs = DNFProcessor.process(event)
        procedures.extend(dnfs)
        serializer = EventProcedureSerializer(procedures, many=True) 

        return Response(serializer.data)

def index(request):
    return render(request, 'index.html', context=dict())
