Préparation réunion 8 juillet
------------------------------



# Services / API de routage

1) OpenRouteService

- demo: https://maps.openrouteservice.org
- as web service: https://openrouteservice.org/services/, see also https://openrouteservice.org/dev/#/api-docs/v2/directions/{profile}/get
- git: https://github.com/GIScience/openrouteservice
   => Java, forked from graphhopper. 
- option de tracer un chemin à travers tout? cfr http://k1z.blog.uni-heidelberg.de/2020/04/01/openrouteservice-maps-%E2%80%93-alternative-routes-roundtrips-and-more-%E2%80%A6/. comment l'utiliser? Sur l'interface maps, on peut utiliser un point "direct"



Créé un token et une API key avec mon compte github

key: 5b3ce3597851110001cf6248d35391925fe04b8792859894153cde3c

ex: 

Tests sur https://openrouteservice.org/dev/#/api-docs/directions/get

https://api.openrouteservice.org/directions?api_key=5b3ce3597851110001cf6248d35391925fe04b8792859894153cde3c&coordinates=5.558825,49.75471%7C5.542259,49.755639&profile=cycling-mountain&preference=fastest&extra_info=surface

https://api.openrouteservice.org/directions?api_key=5b3ce3597851110001cf6248d35391925fe04b8792859894153cde3c&coordinates=5.558825,49.75471%7C5.542259,49.755639&profile=foot-walking&preference=shortest&format=geojson


Test avec le viewer avec des direct points: https://maps.openrouteservice.org/directions?n1=49.756942&n2=5.552473&n3=16&a=49.754793,5.559146,49.753615,5.556185,49.755417,5.551529,49.755056,5.550628,49.75446,5.548525&b=2&c=0&k1=en-US&k2=km&s=1,2,3



2) GraphHopper

 - https://github.com/graphhopper/graphhopper


