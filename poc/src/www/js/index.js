import "@fortawesome/fontawesome-free/scss/fontawesome";
import "@fortawesome/fontawesome-free/scss/solid";

import {Map, View} from 'ol';
import Group from 'ol/layer/Group';
import LayerSwitcher from 'ol-layerswitcher';
import 'ol/ol.css';
import 'ol-layerswitcher/src/ol-layerswitcher.css';

import { addEventsPanels } from './events/panels';
import { gpxFormEvents } from "./events/upload";
import { routingFormEvents } from "./events/routing";
import { itineraryLayer, markerLayer } from './map/layers';
import { ortho, osm, ignOverlay, otm } from "./map/base-layers";
import { mapEvents } from "./events/map";

import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

import '../scss/index.scss';
import '../scss/map.scss';
import '../scss/buttons.scss';
import '../scss/routing.scss';

export const map = new Map({
    target: 'map',
    layers: [
        new Group({
            title: 'Fonds de cartes',
            layers: [ortho, otm, osm]
        }),
        new Group({
            visible: true,
            title: 'Itinéraires',
            layers: [
                itineraryLayer, 
                markerLayer,
                ignOverlay
            ],
        })
    ],
    view: new View({
        center: [556597, 6446275],
        zoom: 9
    })
});

const layerSwitcher = new LayerSwitcher();
map.addControl(layerSwitcher);

mapEvents(map);

addEventsPanels();

gpxFormEvents();

routingFormEvents();
