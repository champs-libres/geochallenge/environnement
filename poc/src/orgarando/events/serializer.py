from rest_framework import serializers
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField
from rest_framework_gis.fields import GeometryField
from .models import Event
from .processor import EventProcedure

class ProjectedGeometrySerializerField(GeometryField):
    """
    Transform a geometry from SRID 3857 (representation data)
    to 31370 (internal value)
    """

    def to_representation(self, value):

        if isinstance(value, dict) or value is None:
            return value

        cloned = value.clone()
        cloned.transform(3857)

        return super().to_representation(cloned)

    def to_internal_value(self, value):
        geom = super().to_internal_value(value)
        geom.srid = 3857
        geom.transform(31370)
        return geom


class EventSerializer(serializers.HyperlinkedModelSerializer):

    itinerary = ProjectedGeometrySerializerField()

    class Meta:
        model = Event
        fields = [ 'id', 'itinerary', 'createdAt', 'eventDateStart', 'eventDateEnd', 'eventType' ]


class EventProcedureSerializer(serializers.Serializer):

    procedureType = serializers.CharField()
    title = serializers.CharField()
    description = serializers.CharField()
    contact = serializers.CharField()
    tel = serializers.CharField()
    email = serializers.CharField()
    options = serializers.DictField()
    geom = ProjectedGeometrySerializerField() 

    class Meta:
        model = EventProcedure
        fields = [ 
                'procedureType',
                'title',
                'description',
                'geom',
                'contact',
                'tel',
                'email',
                'options'
                ]

