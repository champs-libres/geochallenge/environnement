\newpage

## Données mobilisées et exploitation des données {#sec:donnees}


Dans cette section, nous décrivons les données et leur exploitation dans l'application. 

### Données et services du géoportail

#### Fonds de plan

Dans les fonds cartographiques proposés par l'application, il y a une couche d'images aériennes à haute résolution. Cette couche permettra à l'utilisateur de mieux appréhender le terrain. Nous utilisons le service "ORTHO_LAST" des dernières images aériennes disponibles par Web Map Service (WMS).

- ORTHO_LAST, cfr <http://geoportail.wallonie.be/catalogue/e2a615fe-7a2c-4eb3-9dc3-63f466538dda.html>

#### Intersections avec des limites administratives

Une information centrale de notre application est de savoir par quelles communes et quels cantonnements DNF passent les itinéraires que l'organisateur a généré. 

Pour les communes, nous utilisons la couche des données communales du géoportail (cfr <http://geoportail.wallonie.be/catalogue/4bef2af5-422a-45b0-9e9a-fa692fb0f18c.html>). 

Pour les limites de cantonnement, nous utilisons la couche des limites administratives du DNF (cfr <http://geoportail.wallonie.be/catalogue/78bebbe4-dffd-431a-83d2-3460c40b92ff.html>).


Ces deux couches de données sont utilisées en back-end pour calculer les intersections avec les itinéraires. 


#### Contraintes spatiales éventuelles

D'autres couches du géoportail peuvent indiquer des contraintes spatiales à l'organisation de manifestations sportives:

- Zones de tiques en Wallonie: <http://geoportail.wallonie.be/catalogue/58ff142a-9cc9-4ab5-b759-b6939af2353b.html>
- Réseau Natura 2000: <http://geoportail.wallonie.be/catalogue/80a837d8-2c0b-4f77-b5d5-824e9780a4ae.html>
- Conservation de la nature: <http://geoportail.wallonie.be/catalogue/435c454c-0d4b-41cf-a136-a1aba134d9ac.html>
- Zones d'exclusion liée à la peste porcine africaine (PPA): <http://geoportail.wallonie.be/catalogue/f3a03dbd-ae44-48ec-a544-dafdd8409474.html>
- Zones de chasse (quand disponible)
- Zones de quiétude (quand disponible)

Ces couches, nommées par la suite "couches d'exclusion" seraient affichées directement dans l'application et les itinéraires ne pourraient pas passer par les zones d'exclusion dans les cas où la circulation est interdite dans ces zones (chasse, zone de quiétude). Dans les cas où la circulation serait déconseillé, des avertissements seraient éventuellement affichés. 

Nous avons particulièrement mesuré lors de notre préparation du projet le besoin pour une couche d'information sur les chasses et sur les interdictions de circulation qui en découlent (lors des battues). Cette couche d'information devrait être dynamique: les zones de battues évoluant d'un jour à l'autre durant la saison d'ouverture de la chasse. Malheureusement, cette couche n'est pas disponible pour l'instant. Il est à noter toutefois qu'une initiative citoyenne collecte ces données de chasse auprès des cantonnements et les affiche sur un portail web (<http://www.chasseengaume.be/>). Par ailleurs, une telle information est disponible dans des pays voisins comme au grand-duché de Luxembourg (<https://geocatalogue.geoportail.lu/geonetwork/srv/fre/catalog.search#/metadata/cd5c65e3-ce29-4317-bc07-127bc679861f>).  


#### Autres informations utiles

D'autres informations utiles pourraient être affichées sur la carte: 

- RAVeL et Véloroutes en Wallonie - Série: <http://geoportail.wallonie.be/catalogue/ff71a317-fc05-4d0d-b30b-58a1740eb314.html>
- Itinéraires de RAVeL et Véloroutes en Wallonie <http://geoportail.wallonie.be/catalogue/e08bd187-c2e0-4c55-b262-226e0b27e486.html>
- Les Forêts d’Ardenne - Massifs forestiers <http://geoportail.wallonie.be/catalogue/6babbdbf-a3d6-4c1d-96af-c43264a35ae3.html>
- Réseaux d'itinéraires balisés reconnus par le CGT (quand disponible)
- Aire de pique-nique / BBQ (données DNF)

#### API du relief

Nous avons implémenté le calcul du profil d'altitude correspondant à chaque itinéraire en utilisant le service de données de profil d'altitude du géoportail. Ce service est disponible à l'adresse suivante: <http://geoservices.wallonie.be/arcgis/rest/services/GEOTRAITEMENT/Profil/GPServer>. 

De nombreuses API gratuites permettent d'avoir un profil d'altitude le long d'un chemin, mais ces API sont généralement basées sur des données d'altimétrie globale à faible résolution spatiale. Par exemple, le service de profil altimétrique d'openrouteservice.org utilise des données du [SRTM](https://www2.jpl.nasa.gov/srtm/) à ~30 m de résolution. Il en résulte que les altitudes en chaque point sont approximatives et les calculs de dénivellés cumulés sont faussés. En guise d'illustration, voilà ce que donne un itinéraire depuis la Gare de Namur à la Citadelle de Namur avec le profil altimétrique donné sur maps.openrouteservice.org: sur 2.2 km de trajet, le dénivelé cumulé ascendant est de 66 m et celui descendant est de 16 m. 

![Profil d'altitude calculé selon l'API d'OpenRouteService basé sur le SRTM, pour un trajet entre la gare de Namur et la citadelle de Namur](img/profile_ors.png)

Or, en utilisant le calculateur de profil altimétrique du SPW, pour le même itinéraire, nous obtenons un dénivellé cumulé ascendant de 126 m et descendant de 63 m. Les altitudes minimales et maximales du trajet peuvent également être déterminée avec précision, ce qui est souvent évité avec les services globaux par manque de précision des données sous-jacentes. Cet exemple montre bien l'intérêt de l'utilisation de données régionales précises par rapport à un service global. 

![Profil d'altitude calculé selon l'API du SPW basé sur un MNT à 1 m de résolution, pour un trajet entre la gare de Namur et la citadelle de Namur ](img/profile_spw.png)




### Autres données et services

#### OpenStreetMap

La base de données collaborative OpenStreetMap (OSM) est utilisée d'une part pour afficher un fond de plan cartographique à l'application et d'autre part pour calculer les itinéraires. Comme la même base de données est utilisée pour ces 2 fonctions, il n'y a pas de discordance entre l'affichage des chemins sur le fond de plan et les itinéraires. 

Pour le fond de plan, des tuiles d'openstreetmap.org sont utilisés. En cas de charge plus importante pour l'application, un serveur de tuiles dédié pourrait être utilisé. 

Pour le calcul d'itinéraires, les données OSM sont utilisées via l'API d'OpenRouteService. 


#### OpenRouteService

L'API gratuite d'OpenRouteService est utilisée pour calculer les itinéraires. Cette API peut être utilisée gratuitement tant que la limite de 2000 requêtes par jour n'est pas dépassée (cfr <https://openrouteservice.org/plans/>). 


#### IGN

Des fonds cartographique de l'IGN (i.e., cartoweb_overlay) sont également proposés dans l'application (cfr <https://www.ngi.be/website/fr/online-resources/cartoweb-be/>). Largement utilisé par les services de le DNF, la conception de cartes avec ces fonds pourraient être requise par certains cantonnements. 


