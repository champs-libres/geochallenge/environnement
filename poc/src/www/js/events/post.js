import { itinerarySource } from "../map/layers";
import { getProcedure } from "../utils/procedures";


const getCookie = (name) => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

export const saveItinerary = (_e) => {
    /**
    * Post the itinerary as a geojson to the server
    */

    const url = 'http://localhost:18000/api/events/'; 

    const coordinates = itinerarySource.getFeatures()[0].getGeometry().getCoordinates();

    const itinerary = {
        "type": "LineString",
        "coordinates": coordinates
    };
    const data = {
        itinerary: itinerary,
        eventDateStart: '2020-10-03',
        eventDateEnd: '2020-10-03',
        eventType: 'None'
    };

    fetch(url, {
        method: "POST",
        credentials: "same-origin",
        headers: {
            "X-CSRFToken": getCookie("csrftoken"),
            "Accept": "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify(data),
    }).then(response => {
        if (response.ok) {
            response.json().then(
                r =>  getProcedure(r['id'])
            )
        } else {
            console.log('POST failed');
        }
    })
    .catch(error => {
        console.log(error)
    });
};