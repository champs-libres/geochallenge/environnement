import {Modify, Snap, Select} from 'ol/interaction';
import { transform } from 'ol/proj';

import { markerSource, itinerarySource, getWayPoints, addWayPointAntepenultian } from "./layers";
import { doRouting, updateRoutingFormWithCoordinates } from '../routing/routing';
import { uuidv4 } from '../utils/uid';


const interactionModifyMarker = new Modify({
    source: markerSource
});

const interactionSnapMarker = new Snap({
    source: markerSource
});

const interactionSelectMarker = new Select({
    source: markerSource
});

const interactionModifyItinerary = new Modify({
    source: itinerarySource
});

const interactionSnapItinerary = new Snap({
    source: itinerarySource
});

export const addModifyInteraction = (map) => {
    map.addInteraction(interactionSelectMarker);
    map.addInteraction(interactionModifyMarker);
    map.addInteraction(interactionSnapMarker);
    map.addInteraction(interactionModifyItinerary);
    map.addInteraction(interactionSnapItinerary);
};


const getSelectedMarker = () => interactionSelectMarker.getFeatures()[0];

export const onMarkerChange = () => {

    interactionModifyMarker.on('modifyend', e => {
        // const marker = getSelectedMarker();
        // console.log(marker)
        doRouting(getWayPoints());
    });
};



export const onItineraryChange = () => {

    interactionModifyItinerary.on('modifyend', e => {
        // const marker = getSelectedMarker();
        // console.log(marker)
        const id = 100; //TODO or getWayPoints().length 
        // addRoutingInput(id, uuidv4());
        const coordinates = e.mapBrowserEvent.coordinate;
     
        const uid = uuidv4();
        addWayPointAntepenultian(coordinates, uid); //TO DO il faudrait ajouter le point à l'intérieur de la collection de wayPoints, et pas à la fin. 
        const coordinatesWGS84 = transform(coordinates, 'EPSG:3857', 'EPSG:4326');
        updateRoutingFormWithCoordinates(coordinatesWGS84, uid); //TODO, pass the right id en fonction de l'endroit du point!
  
        doRouting(getWayPoints());
    });
    // TODO: get the new point of the change and put it to the markerLayer ?
    //make an new input field and fill it ?, 
    // and doRouting ?
    // OR simply modify the line? 
};

//TODO: select a marker and delete it. 