\newpage

## Annexe 3: Question et réponse quant à la license des données d'itinéraires produites à partir de données OpenStreetMap {#sec:annexe3}

\textbf{Question envoyée à legal@osm.org}

\setlength{\leftskip}{0.5cm}
Hello, 

I have a use case that I think is very common but I could not find on the osmfoundation.org/wiki/Licence/ pages any information about it:

Suppose I build a website with a map and a routing engine such as OpenRouteService that is using OSM data. The routing engine is used by users to generate routes data from a point A to a point B. These data can then be exported in GPX or geoJSON or any suitable format. Can these data (eg the GPX files) be shared under another licence that the oDbL? Does it trigger the Share-alike obligations?

Actual implementations of this issue already exist: <https://maps.openrouteservice.org>, cycle.travel, and many others. 

I found that geocoding outputs (see here <https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines/Geocoding_-_Guideline>) from OSM data can be shared under another licence than the oDbL and that it does NOT trigger the share-alike obligation. I guess this is the same in my case, but I need to be sure of that and I think this information could be added in the osmfoundation.org/wiki/Licence/ pages. 

Many thanks in advance!

Julien Minet, aka juminet

\setlength{\leftskip}{0pt}

\textbf{Réponse}

\setlength{\leftskip}{0.5cm}

Dear Julien,

we would very likely this consider this a produced work, however please see <https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines/Produced_Work_-_Guideline>  on using a Produced Work for extracting OSM data.

Kind regards

Simon Poole

-- 

OpenStreetMap Foundation
St John’s Innovation Centre
Cowley Road
Cambridge
CB4 0WS
United Kingdom
A company limited by guarantee, registered in England and Wales.
Registration No. 05912761. 

\setlength{\leftskip}{0pt}


