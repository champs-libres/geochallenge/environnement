from events.models import Event
from events.processor import EventProcessor, EventProcedure
from django.db import connection
from django.contrib.gis.geos import GEOSGeometry

class DNFProcessor:

    _QUERY = """
    SELECT
	canton,
	ingenieur,
	grade,
	tel,
	email,
	ST_AsEWKB(ST_Intersection(cantonnements.geom, event.itinerary)) AS geom
FROM dnf.cantonnements
JOIN public.events_event AS event ON ST_Intersects(cantonnements.geom, event.itinerary)
WHERE
    event.id = %s
    """

    def process(event):
        procedures = list()
        with connection.cursor() as cursor:
            cursor.execute(DNFProcessor._QUERY, [ event.id ])
            for row in cursor.fetchall():

                procedure = EventProcedure(
                        procedureType="dnf_cantonnement",
                        geom=GEOSGeometry(row[5]),
                        tel=row[3],
                        email=row[4],
                        contact=row[1] + ", " + row[2],
                        description="""
L'itinéraire traverse le cantonnement de {}.

Une autorisation est nécessaire.

Vous pouvez la demander auprès de {}, {}.

""".format(row[0], row[1], row[2]),
                        title="Traversée du cantonnement DNF de {}".format(row[0]),
                        options={ 'ingenieur': row[1], 'grade': row[2], 'tel': row[3], 'email': row[4], 'canton': row[0]}
                        )
                procedures.append(procedure)
  
        return procedures

