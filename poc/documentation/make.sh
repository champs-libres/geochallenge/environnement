#!/bin/bash

pandoc 0*.md -o documentation.pdf --pdf-engine=xelatex --template=latex.template --number-sections
