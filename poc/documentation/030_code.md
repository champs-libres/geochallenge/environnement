\newpage

# Documentation technique

## Installation

Le code source de cette application est disponible sur le dépôt <https://gitlab.com/champs-libres/geochallenge/environnement>. 

Comme indiqué dans le README de ce dépôt, l'application peut être lancée à l'aide de `docker`:

`docker-compose up`

Ce composeur docker fait fonctionner plusieurs conteneurs:

\begin{table}[hbtp]
\small
\begin{tabular}{|p{3cm}|p{3cm}|p{1.2cm}|p{5cm}|}
\hline
\textbf{nom du conteneur} & \textbf{description} & \textbf{port} & \textbf{accès} \\
\hline
db & base de données & 5496 & psql -h 127.0.0.1 -U postgres -w postgres -p 5496 \\
python & application Django & 18000 & http://localhost:18000  \\
node & application js & 8002 & http://localhost:8002  \\
\hline
\end{tabular}
\caption{Conteneurs docker de l'application}
\end{table}


Pour l'installation, effectuer les commandes suivantes: 

1. contruire le conteneur python-django: `docker-compose build python`
2. lancer le conteneur: `docker-compose up -d`
3. appliquer les migrations: `docker-compose exec --user $(id -u) python /app/orgarando/manage.py migrate`



## Architecture

\begin{figure}[hbtp]
  \caption{Schéma de l'architecture de l'application}
  \centering
    \includegraphics[width=\textwidth]{./img/archi.pdf}
\end{figure}

L'architecture de l'application repose sur la combinaison d'une base de données PostgreSQL + PostGIS, d'un back-end en Python Django et du front-end. 

La base de données stocke les données de l'événement sportif, du ou des tracés se rattachant à cet événement, et des couches géographiques du découpage administratif (cantonnement DNF, communes). Ces couches administratives contiennent les informations de contact à afficher après l'intersection du tracé et des découpages administratifs. Par la suite, d'autres couches de contraintes spatiales (zones de quiétude, zones de chasse, ...) pourront être stockées en base de données, ou appelées depuis les services web du SPW. 

Le back-end gére les modèles des entités (événements, tracés, utilisateurs, couches géographiques, ...). Il réalise également l'intersection entre les tracés et les découpages administratifs. 

Le front-end comporte les fichiers JS et SASS qui sont compilés pour servir l'application. 

## Technologies

### Back-end

Le back-end utilise le framework Python Django avec une base de données PostgreSQL + PostGIS. Parmi les dépendances, citons GeoDjango, Django Rest Framework et rest_framework_gis. 

Les entités définies à ce stade sont les événements sportifs qui comportent un itinéraire et les couches administratives. Les géométries sont stockées en Lambert Belge 1972 (EPSG:31370) mais sont reprojetés en Web mercator (EPSG:3857) vers la carte dynamique. Les intersections sont réalisées avec la fonction PostGIS `ST_Intersection` avec les géométries projetées en Lambert Belge 1972. 

Le résultat des intersections (i.e., les procédures administratives) est servi par Django Rest Framework. 

Enfin, l'application Django sert le front end au moyen d'un template unique (`index.html`).

### Front-end

La plupart des fonctionnalités de l'application sont écrites du côté front-end. Le squelette HTML de l'application est minimal: l'essentiel du DOM est construit via des fonctions javascript déclenchées sur base d'événements. 

La librairie `OpenLayers` permet de gérer l'affichage des couches géographiques et toutes les fonctionnalités de la carte dynamique. Le calcul d'itinéraire est effectué par la librairie `openrouteservice` et son API gratuite. 

#### Conventions de programmation


Le code est écrit en suivant au minimum la syntaxe spécifiée par l'ECMAScript 6. Il est compilé en js non minifié à l'aide de l'empaqueteur `parcel`. 

Les fichiers sont organisés dans des dossiers différents et les fonctions appelées à l'aide d'imports. Les imports sont triés: d'abord les imports de librairies externes puis les imports internes. 

Les fonctions sont décrites suivant la syntaxe JSDOC quand cela s'avère nécessaire, notamment pour spécifier la signature et le type de données d'entrées et de sortie des fonctions. 

Côté front-end, il y a les dépendances suivantes: 

  - OpenLayers
  - OpenLayers-layerSwitcher
  - fontawesome
  - d3plus-plot
  - toaster-js
  - openrouteservice

Les dépendances à des librairies externes sont évitées autant que possible. 

Le style est écrit en `Sass` et compilé à l'aide de `parcel`. 

#### Services externes

L'application front utilise plusieurs services externes qui fournissent des données:

- Couches de tuiles XYZ et WMS: Des fonds de carte sont affichés dans la carte de l'application pour permettre à l'utilisateur de se repérer dans l'espace. Une partie de ces fonds sont des services de cartes tuilés de type "XYZ". Comme indiqué dans la section \ref{sec:donnees}, la carte affiche également un WMS du SPW (ORTHO_LAST) combiné avec une surcouche semi-transparente de labels de l'IGN (cartoweb_overlay) pour fournir un fond d'image aérienne. 

- Service de géocodage: L'application utilise du géocodage (i.e., transformation d'un nom de lieu en coordonnées géographiques) dans le formulaire de routage. Ce service permet de rentrer un lieu pour choisir des points de passages du tracé. Ce service est fourni par l'API OpenRouteService. 

- Service de routage: Le service de routage utilisé est l'API OpenRouteService (cfr \ref{sec:donnees}). Ce service se base sur les données d'OpenStreetMap. Il propose divers profils pour le calcul d'itinéraire: voiture, camion, vélo, VTT, vélo électrique, piéton, ... Entre 2 points donnés, le meilleur trajet calculé dépend de ces profils. 

- Service de profil altimétrique: Le service de profil altimétrique du SPW est utilisé. Comme indiqué dans la section \ref{sec:donnees}, l'utilisation de ce profil apporte une bien meilleure précision pour le calcul du profil altimétrique le long du tracé ainsi que les statistiques dérivées (dénivellés cumulés, altitudes minimale et maximale). 