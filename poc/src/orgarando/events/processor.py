from .models import Event

class EventProcessor:
    def process(self, event, **kwargs):
        pass

class EventProcedure:
    procedureType = None
    title = None
    description = None
    geom = None
    contact = None
    tel = None
    email = None
    options = {}

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        


