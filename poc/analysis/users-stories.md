Users stories pour l'appli organisation-rando
---------------------------------------------

# Users

1) L'organisateur·trice de randonnée, Sarah. Par randonnée, on entend marche à pied, vélo, VTT, courses à pieds et autres types d'activités extérieures
2) le fonctionnaire communal en charge de l'acceptation du dossier de l'événement, Natacha
3) le fonctionnaire DNF en charge de la validation et de l'acceptation du tracé de l'événemenent, Manu.
4) le fonctionnaire SPW en charge de l'acceptation des traversées de voiries, Robert

Users optionnels
5) L'utilisateur grand public qui utilise le site pour planifier sa randonnée

# Users stories

## User story 0: Sarah doit se créer un compte dans l'application

### Etapes 
* Sarah arrive sur l'application via un navigateur internet
* Sarah crée un compte sur l'application (ou pas?)

### Ecrans
* homepage
* sign in/log in


## User story 1: Sarah créée un événement
### Etapes
* Sarah, loggée, accéde à son espace utilisateur (cela peut être sur la carte principale)
* Sarah crée un événement avec les infos suivantes
   - le nom
   - la ou les dates 

### Ecrans
* main - espace utilisateur - événement


## User story 2a: Sarah téléverse un itinéraire pour son événement

### Etapes
* Sarah arrive sur la carte principale
* Sarah téléverse son itinéraire au format adéquat (GPX, geojson, ...) et ajoute les infos suivantes:
    - le type d'activité
    - son nom
* Quand il est fini, elle l'ajoute à son événement dans son espace utilisateur

### Ecrans
* main - edition tracé
* main - espace utilisateur - tracé(s)


## User story 2b: Sarah trace un itinéraire pour son événement

### Etapes
* Sarah arrive sur la carte principale
* Sarah encode son itinéraire avec les infos suivantes (peuvent être entrées dans le désordre)
    - le tracé (MULTILINESTRING)
    - le type d'activité (= profil d'utilisateur: marche, vtt, etc.)
    - son nom
* Quand il est fini, elle l'ajoute à son événement dans son espace utilisateur

### Ecrans
* main - edition tracé
* main - espace utilisateur - tracé(s)

### Details des fonctionnalités

* le tracé se fait en définissant un point A et un point B. Ces points peuvent définis au click sur la carte ou bien dans un formulaire
* Voir si on implémente une fonction roundtrip (et laquelle)

## User story 3: Sarah veut savoir quelles sont les contraintes d'exclusion spatiale 

### Etapes
* Sarah est en train de tracer son itinéraire ou bien a importé son itinéraire
* Son tracé intersecte des zones d'exclusions (ou pas): un récapitulatif des intersections apparait OU il lui est impossible de tracer dans les zones d'exclusion. Suivant le résultat, elle peut modifier 

### Ecrans
* main - edition tracé


## User story 4: Sarah veut (enfin, doit) partager son événement aux administrations concernées
* Sarah, loggée, accéde à son espace utilisateur
* Les administrations concernées par les tracés de son événement sont listées (directement, ou en appuyant sur un bouton)
* les moyens de contacter les administrations sont affichés (ou on peut les notifier directement avec un bouton)






### Entités

1) L'événement 

L'événement ou la manifestation sportive de l'organisateur. 

Il peut avoir plusieurs tracés. 
Il a un nom
Il a une date de début et une date de fin 
Il a un organisateur (un propriétaire)
C'est un événement ADEPS (oui/non)

2) Le tracé

Il a un nom (le kilométrage par défaut)
Il a une info géographique de type MULTILINESTRING
Il a un type d'activité (de sport): marche à pied, vélo-cyclo, VTT, course à pied, ...
Il a des infos géographiques dérivées: longueur, info de profil altimétrique, info de surface de OpenRouteService, ...

3) La carte

Il a un nom
Il a un ou plusieurs tracés

4) Contraintes d'exclusion

5) User

# Questions

1) Authentification

Est-ce que Sarah doit se créer un compte? Si oui, facilité pour retrouver ses données. Sinon, envoi de ses données via un lien avec un hash. 







