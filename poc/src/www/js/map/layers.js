import GeoJSON from 'ol/format/GeoJSON';
import {Vector as VectorSource} from 'ol/source';
import {Vector as VectorLayer} from 'ol/layer';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import { transform } from 'ol/proj';
import {Stroke, Style, Icon } from 'ol/style';

import { map } from '../index';
import mapIcon from '../../img/map-pointer.png';


export const itinerarySource = new VectorSource({
    format: new GeoJSON({
        dataProjection: 'EPSG:4326', 
        featureProjection: 'EPSG:3857' 
    })
});

export const itineraryLayer = new VectorLayer({
    source: itinerarySource,
    title: 'Itinéraire',
    style: new Style({
        stroke: new Stroke({
            color: 'rgba(180, 10, 10, 0.7)', // red
            width: 5
        })
    })
});

export const updateItinerary = (json) => {
    itinerarySource.clear();
    itinerarySource.addFeatures(
        new GeoJSON({ 
            dataProjection: 'EPSG:4326', 
            featureProjection: 'EPSG:3857' 
        }).readFeatures(json)
    );
    zoomToItineraryLayer();
};


export const markerSource = new VectorSource({});

export const markerLayer = new VectorLayer({
    source: markerSource,
    title: 'Way points',
    style: new Style({
        image: new Icon({
            anchor: [0.5, 46],
            anchorXUnits: 'fraction',
            anchorYUnits: 'pixels',
            //src: mapIcon,
            src: 'http://localhost:18000/static/map-pointer.11013935.png',
            scale: 0.4
        }),
    })
});

export const addWayPoint = (coordinates, uid) => {
    /**
    * Add a wayPoint to the map
    * @param {number[]} coordinates - coordinates [x, y] of the point
    * @param {number} uid - a unique id
    */

    const marker = new Feature({
        //type: 'icon',
        geometry: new Point(coordinates),
        uid: uid
    });
    markerSource.addFeature(marker);
};


export const addWayPointAntepenultian = (coordinates, uid) => {
    /**
    * Add a wayPoint to the map at the antepenultian position of the way points collection
    * @param {number[]} coordinates - coordinates [x, y] of the point
    * @param {number} uid - a unique id
    */

    const marker = new Feature({
        //type: 'icon',
        geometry: new Point(coordinates),
        uid: uid
    });
    let features = markerSource.getFeatures();
    features.splice(features.length-1, 0, marker);
    markerSource.clear();
    markerSource.addFeatures(features);
};

export const removeWayPoint = (uid) => {
    /**
    * Remove the way point from the map
    * @param {number} uid - uid of the way point
    */
    markerSource.forEachFeature(f => {
        if (f.get('uid') === uid) {
            markerSource.removeFeature(f);
        };
    })
};

export const getWayPoints = () => {
    /**
    * Returns the way points as an array in EPSG:4326
    */
    let wayPoints = [];

    markerSource.forEachFeature(f => {
        wayPoints.push(transform(f.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326'));
    });
    return wayPoints;
};

export const getLayerByTitle = (title) => {
    /**
    *  Returns a layer by its title, for layers that are within a layer group.
    *  @param {string} title - The exact title of the layer
    */
    let layer = undefined;
    map.getLayers().forEach(g => {
        if (typeof g.getLayers === 'function') {
            g.getLayers().forEach(
                l => {
                    if (l.get('title') === title) {
                        layer = l;
                    }
                }
            )
        }
    })
    return layer;
}

export const zoomToItineraryLayer = () => {
    console.log(getWayPoints())
    if (getWayPoints().length <= 3) { // avoid rezooming when we add nodes
        console.log('zoom to itinerary')
        const extent = itinerarySource.getExtent();
        map.getView().fit(extent, {padding: [300, 300, 300, 600]});
    }
};