\newpage

## Questions légales


### Licence du code produit 

Le code de cette application est licencé sous la licence General Public Licence ([GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html)).

### Propriété intellectuelle des données générées

Les données OpenStreetMap (OSM) sont utilisées dans cette application pour générer les itinéraires. Les données OSM étant sous licence oDbL (<https://opendatacommons.org/licenses/odbl/>), est-ce que les données d'itinéraires générées devront être sitribuées sous cette même licence? 


Cette page <https://wiki.osmfoundation.org/wiki/Licence/Licence_and_Legal_FAQ#What_exactly_do_I_need_to_share.3F,> semble indiquer qu'une route digitalisée à partir de données OSM est un "produced work", qui peut être licencé avec une autre licence que l'ODbL, et qui ne déclenche pas la condition de partage sous la même licence. "In ODbL parlance, this is known as a "Produced Work". Un autre document stipule que le résultat de géocodage n'est pas un extrait de la BD OSM: <https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines/Geocoding_-_Guideline.> Cela ne déclenche pas la condition de partage sous la même licence (It does not trigger the Share-alike obligations)

C'est par ailleurs la réponse que nous avons eu lorsque nous avons posé la question à la fondation OpenStreetMap (cfr Annexe 3, page \pageref{sec:annexe3}). 

En outre, il est intéressant également de noter qu'il est permis de mélanger des données OSM et des données produites sous une autre licence sans obligation de redistribution (clause de partage "Share-Alike") pour autant que cela soit fait en interne dans une entreprise ou une organisation: cfr <https://wiki.osmfoundation.org/wiki/Licence/Licence_and_Legal_FAQ#Are_there_any_special_conditions_or_restrictions_for_commercial_or_academic_use.3F>, "Share-Alike only applies if you distribute what you have done to outside people or organisations. You can do what you like at home, or in your school, organisation or company ... the following section does not apply to you." Les données OSM peuvent donc être utilisées en interne avec beaucoup de liberté: par exemple des producteurs de données comme le SPW ou l'IGN pourraient utiliser OSM comme outil de comparaison de leur données sans devoir changer la licence de leurs données.

### Conformité au RGPD

Concernant l'utilisation des données OSM, aucune information personelle quant aux contributeurs OSM (usernames, date de contributions) n'est disponible dans l'application. 

Dans le cas où l'application permettra une inscription avec un compte utilisateur, le RGPD s'appliquera aux données personnelles de ces utilisateurs. Il est prévu de spécifier quelle sera l'utilisation des données personnelles et pourquoi ces données personnelles sont collectées (dans ce cas, pour faciliter l'accès à son compte et à ses itinéraires). Ces spécifications seront proposées dans les termes et conditions d'utilisation de l'application. Enfin, les utilisateurs disposeront d'un droit à l'oubli. 