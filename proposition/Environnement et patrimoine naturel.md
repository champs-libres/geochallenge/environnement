Geochallenge - Environnement et patrimoine naturel
--------------------------------------------------


# Aide à l'organisation de parcours de randonnées

Idées d'acronymes: WalOnTrack, WalOnTrek, WalTrek, WalHike

Nous avons justement préparé 2 petites "users stories" qui ne rentraient pas dans les autres cases du formulaire!

Jean-Pierre et sa marche Adeps
------------------------------

Nouvellement élu président de son club de belote, Jean-Pierre est chargé d'organiser une marche Adeps dans l'espoir de récolter des fonds. Après avoir décidé du tracé sur des cartes IGN au cours de longue soirées et après repérage sur le terrain, il apprend qu'il doit demander des autorisations auprès des 2 communes traversées et de la DNF afin de valider son tracé. Jean-Pierre a d'ailleurs des doutes quant à savoir si on peut traverser les terres du baron de Hérode. Lors d'un de ses déplacement à Namur, Jean-Pierre fait des photocopies A1 des cartes IGN concernées puis trace le trajet à l'aide d'un fluo. Après plusieurs allers-retours entre la commune et la DNF et de multiples annotations sur la carte (car il a fallu faire des détours pour éviter les chemins traversant la propriété de de Hérode, ainsi que toute une portion d'un bois qui sera justement en chasse ce jour-là), il doit refaire des photocopies à Namur afin de soumettre sa carte finale. Bien que retraité, Jean-Pierre se dit que soumettre un tracé pour un tel événement est une démarche qui prend pas mal de temps. C'est aussi l'avis de Ludovic, le stagiaire DNF qui s'est empressé d'encoder dans Google Earth le tracé reçu à trois reprises avant d'entendre à chaque fois l'ingénieure lui dire qu'il fallait le changer.


Sarah et son Run&Bike
------------------------

Trailleuse chevronnée, jamais à court d'idée, Sarah décide d'organiser avec son club de running un événement sportif novateur: un Run & Bike d'orientation en Crocs®. Adepte des nouvelles technologies, elle n'a aucun mal à planifier et créer les 7 parcours au format GPX à l'aide de l'application Strava. Par contre, elle tombe des nues lorsqu'elle apprend qu'il faut faire des demandes officielles à toutes les communes traversées (4!) et à la DNF, avec des délais variables et même des supports différents: telle commune exige une copie des cartes papier tandis que les autres communes se satisfont d'une version électronique. Deuxième déconvenue, elle prépare ses 7 cartes avec un outil trouvé en ligne sur un forum: InkAtlas, mais Manu, l'agent DNF qui reçoit les cartes, les lui renvoie car la résolution de la carte ne permet pas de voir si un des tracés passe dans la zone de quiétude de son triage. Habitué des cartes IGN, Manu préférerait d'ailleurs recevoir ces cartes avec un fond IGN. Enfin, dernière déconvenue: lors du balisage la veille de l'événement, Sarah et son équipe s'aperçoivent qu'un balisage d'une marche Adeps pour un club de belote organisée le même jour chevauche ses itinéraires sur plusieurs tronçons.


Ces deux mises en situations illustrent le besoin que nous souhaitons combler: une application de cartographie pour l'organisation de manifestations sportives.


De très nombreuses manifestations sportives sont organisées chaque année en Wallonie: promenades à pied, à VTT, courses à pied, etc. Ces manifestations, souvent organisées par des bénévoles, requièrent des autorisations des communes concernées ainsi que du service du Département Nature et Forêt local. Il n'existe pas à ce jour de formulaire unique en Wallonie pour transmettre les tracés des parcours demandés à ces administrations. Les organisateurs sont en outre souvent dépourvus d'information quant aux zones où ce type d'événement est proscrit. Nous proposons de créer un outil web cartographique à destination des organisateurs de manifestations sportives pour créer et transmettre les tracés des parcours sous forme numérique et/ou papier. Cet outil proposera le réseau de chemins et sentiers accessibles pour organiser une randonnée, en informant sur les **contraintes d'exclusion** et en apportant d'autres informations utiles. L'organisateur pourra créer son itinéraire dans une carte interactive. Au final, l'organisateur disposera d'une carte au format pdf à imprimer et/ou à envoyer à la liste des administrations concernées.

Les acteurs identifiés ont des besoins différents:

- La ou les commune(s) concernée(s) par la manifestation a (ont) besoin des informations des tracés afin de compléter le dossier de sécurité de l'événement. En cas de problème, les services de secours doivent être en mesure de savoir où sont les participants de l'événement. La commune peut en outre désirer promouvoir son réseau d'itinéraires balisés aux organisateurs, plutôt que de faire circuler les participants de l'événement sur des chemins moins entretenus.

- Le service DNF local est l'autorité principale quant à l'autorisation des choix des tracés. Celui-ci peut exclure des parties des parcours proposés en fonction de contraintes d'exclusion que l'organisateur pourrait connaitre mais qui ne sont pas facilement accessibles (chemins privés, zones de quiétude, fermeture temporaire pour cause de chasse, ...) ou même que l'organisateur ne peut connaitre à l'avance (zone de nidification d'espèces rares, ...). Ces informations peuvent varier d'une année à l'autre. Il doit également veiller à la coexistence des multiples activités ayant lieu en forêt: vie sauvage, chasse, manifestations sportives, exploitation forestière, etc.

- L'organisateur de l'événement a des compétences en cartographie très variables. Il utilise souvent des outils disponibles sur le web pour créer ses parcours ou bien des cartes papiers (IGN). Il veut une procédure la plus simple possible et disposer d'un maximum d'information préalablement à sa demande (quant aux contraintes d'exclusion). Il veut également savoir quels sont les itinéraires des événements sportifs déjà prévus au même moment que son événement. Il est éventuellement intéressé par avoir un fichier à imprimer à haute résolution de la carte des parcours de son événement, afin de l'afficher le jour de l'événement, ainsi que d'une webmap et des fichiers GPX à intégrer dans son site web ou à distribuer sur les réseaux sociaux.

À terme, la solution développée sera une application web qui devra être maintenue par une autorité qu'il reste à identifier. Celle-ci devra pouvoir administrer l'application afin de la faire évoluer avec des données variables dans le temps.



## Idées à creuser

- Si Rallye automobile / balade moto, y a t il besoin de savoir si il faut demander autorisation à la commune ou à la région (ce qui peut aider : données du réseau routier régio http://geoportail.wallonie.be/catalogue/bdcb789c-4b02-4c0c-863a-98dac4ed0240.html  - http://geoportail.wallonie.be/catalogue/164caaa0-fb9f-4ff7-a755-cf60b042dde3.html )

- Pour ce qui est d'événement cyclo / piéton :
    - mettre en avant pistes cyclables
    - passage piétons ()
    - trottoir
    - réseau voies lentes
    - réseau points noeuds


- Par la suite calcul dénivelé (?)
- Export en KML / GPX à donner aux gens
- à voir: Routing without paths: http://k1z.blog.uni-heidelberg.de/2020/04/01/openrouteservice-maps-%E2%80%93-alternative-routes-roundtrips-and-more-%E2%80%A6/
- Aussi attention à la sécurité : Versants avec une pente supérieure à 30 degrés  ( http://geoportail.wallonie.be/catalogue/f4e3709c-fdda-452e-b723-7ad55e584f1f.html )

# TODO

- contact O. Barthelemy pour parler de l'idée: ok
    -> Il y a aussi les rally moteurs qui sont concernés
- contact M Dupuis: OK
- contact Catherine Colson:

réponse:

- Le problème auquel on est parfois confronté dans le Hainaut, c’est la traversée des RND (la carte des sites sous statut de protection se trouve aussi sur le géoportail :conservation de la nature).  

 - Il pourrait être utile d’intégrer une demande pour une activité « jeux » en forêt. Le nouveau code forestier impose à toute propriété de plus de 100 ha d’un seul tenant de prévoir une zone d’accès libre aux mouvements de jeunesse. SI ces zones sont en effet prévues et localisées dans nos aménagements forestiers, il n’existe à l’heure actuelle aucune carte officielle de ces zones sur le géoportail. Ces zones sont vouées aux activités de plein air en dehors des chemins : confection de cabanes, parcours fil d’Ariane, … Tu pourrais, dans ton exemple, proposer une petite épreuve à réaliser en forêt pour le run & bike avec des obstacles dans cette zone ?

- Dans le même ordre d’idée, il y a aussi les « zones d’accueil », bien distinctes des zones d’accès libre, ces zones sont équipées de bancs, tables abris et parfois aire de BBQ. L’usage des BBQ demande l’autorisation de l’agent du DNF. Si Jean-Pierre veut clôturer sa marche Adeps en offrant un pain saucisse à ses participants, il serait heureux de connaitre l’emplacement de cette aire de BBQ et les modalités pratiques pour l’utiliser.

- Tu n’y seras pas confronté ici, mais il y a aussi un nouvel engouement pour les bivouacs en forêt. Il s’agit de zones où on peut camper la nuit en forêt soumise et faire un feu. Peu de gens sont au courant mais ces zones sont remises au gout du jour grâce aux  initiatives de valorisation des massifs forestiers. Et pour m’être intéressée à la grande traversée du pays de Chimay en particulier, je peux dire que ces aires sont assez fréquentées. Il pourrait donc être utile d’intégrer ces bivouacs à votre appli. http://www.foretdupaysdechimay.be/fr/page/la-grande-traversee

- Autre point problématique dans nos forêts périurbaines : la coordination des diverses activités planifiées simultanément. Il est fréquent que 2 ou 3 organisations se croisent  ou utilisent des tronçons communs. Si deux clubs de VTT prévoient deux itinéraires différents, il faut veiller à ce qu’ils ne se croisent pas en sens inverses. D’une part pour des raisons évidentes de sécurité et d’autre part pour le balisage temporaire. Si cette appli voit le jour, il serait bon de pouvoir visualiser les demandes de parcours qui sont en cours ou ceux qui sont déjà autorisés.

- Dernière chose, il nous arrive chaque année d’avoir des demandes pour des motocross. Une grosse association nous fait une demande annuelle qui couvre plusieurs cantonnements et plusieurs directions, ce qui nous demande une certaine coordination dans la délivrance de l’autorisation avec les divers chefs de cantonnement et directeurs. Il y a donc le lien à faire avec l’autorité administrative du DNF et l’exclusion des parcours motorisés en forêt bénéficiant du régime forestier. Ces deux contraintes sont donc liées à la carte des propriétés gérées par le DNF (dispo sur le géoportail).



# FORMULAIRE GEOPORTAIL

1. Nom de la structure participante

2.Type de candidature
Monde de l'entreprise (Startup, TPME)
Communauté académique
Secteur associatif
Mixte

3.Statut ou forme juridique de la structure (petite entreprise, moyenne entreprise, asbl, faculté, classe, association momentanée, …)

4.En quelques mots, pouvez-vous présenter votre structure (activité principale, nombre d'années d'existence, projets déjà menés...)

Champs-Libres est une société coopérative de droit belge, à finalité sociale. Elle s'inscrit dans le
développement de l'économie sociale et solidaire, par la fourniture de services lié au logiciel libre aux
associations, entreprises d'économie sociale et aux pouvoirs publics. Champs-Libres a été créée en
2013 et emploie actuellement 4 travailleurs temps-plein. Champs-Libres est spécialisé
dans l'utilisation de QGIS et de PostGIS, ainsi que dans l'utilisation de nombreuses bibliothèques
Python et C (GDAL, Mapnik, ...) et logiciels (GeoServer, ...) dans le domaine de la géomatique, que ce soit 
pour construire des applications métiers ou des portails géographiques. 


5.Adresse de la page web ou du site internet de la structure ?

https://www.champs-libres.coop/

6.Prénom

7.Nom

8.Fonction

9.Adresse e-mail

10.Numéro de téléphone

11.Pour quel défi proposez-vous votre candidature ?
Défi - Mobilité
Défi - Qualité de vie
Défi - Environnement et patrimoine naturel
Défi - Entreprendre

12.Quel est le nom de votre projet ?

Outil d'organisation de manifestations sportives



13.Pouvez-vous résumer en quelques lignes votre projet ainsi que les résultats attendus ?





14.Quelles compétences, expériences et motivations mettez-vous en avant pour mener le projet ?

Nos compétences et expériences

- Expériences pratiques de l'organisation d'événements sportifs: création des circuits, dépôt de dossier aux autorités communales et DNF et balisage sur le terrain

- Contacts avec les utilisateurs: Pour préparer cette proposition, nous l'avons préalablement soumise à des acteurs de terrains: 1) Manu Dupuis, agent DNF à Habay-la-Neuve, 2) Olivier Barthelemy, échevin de la nature, de la forêt et du tourisme de Habay-la-Neuve, 3) Catherine Colson, ingénieure DNF à la Direction de Mons. Ces avis nous ont permis d'affiner la proposition en recueillant les avis d'acteurs de terrain (DNF et autorité communale). Ces personnes-ressources pourront nous aider à affiner la proposition durant le stade de développement.

- Expériences de mise en place de portails cartographiques (cfr https://www.champs-libres.coop/page/webgis/). Nous maitrisons la pile logicielle nécessaire pour développer et maintenir une application web cartographique: serveur, moteur de rendu cartographique (GeoServer, MapServer), client web cartographique (OpenLayers, LeafletJS), langage de programmation web backend et frontend (PHP-Symfony, Python-Django, Typescript, Javascript)

- Bonne connaissance de services de calculs d'itinéraires (GraphHopper, OpenRouteService)

- Très bonne connaissance de l'extraction, le traitement et l'utilisation des réseaux de voiries disponibles sur OpenStreetMap en Belgique.

Nos motivations

- Développer une application utile et agréable, qui pourrait être rapidement disponible aux utilisateurs.

- L'application pourrait aussi servir comme noyau de base pour une application de découverte / planification d'itinéraires pour le secteur touristique en Wallonie. Le marché des applications web et mobiles de randonnées / sport d'extérieurs est un des plus saturés qui soit. Néanmoins, le partage d'informations officielles des autorités publiques peut constituer une originalité pour créer une nouvelle application qui se différencierait des applications déjà disponibles (souvent développé hors de Belgique), d'autant plus si ces informations sont variables dans le temps.



15.Combien de personnes seront affectées à la réalisation du projet ?
de 1 à 3


16.Avez-vous déjà une idée des éventuelles données du Géoportail de la Wallonie que vous allez exploiter ?
Pour consulter le catalogue du Géoportail : http://geoportail.wallonie.be/catalogue-donnees-et-services

Information de base

- Limites communales: http://geoportail.wallonie.be/catalogue/4bef2af5-422a-45b0-9e9a-fa692fb0f18c.html
- Limites administratives du DNF: http://geoportail.wallonie.be/catalogue/78bebbe4-dffd-431a-83d2-3460c40b92ff.html


Contraintes spatiales éventuelles à l'organisation de manifestations sportives

- Zones de tiques en Wallonie: http://geoportail.wallonie.be/catalogue/58ff142a-9cc9-4ab5-b759-b6939af2353b.html
- Réseau Natura 2000: http://geoportail.wallonie.be/catalogue/80a837d8-2c0b-4f77-b5d5-824e9780a4ae.html
- Conservation de la nature: http://geoportail.wallonie.be/catalogue/435c454c-0d4b-41cf-a136-a1aba134d9ac.html
- Zones de chasse (quand disponible)

Autres informations utiles

- RAVeL et Véloroutes en Wallonie - Série: http://geoportail.wallonie.be/catalogue/ff71a317-fc05-4d0d-b30b-58a1740eb314.html
- Itinéraires de RAVeL et Véloroutes en Wallonie http://geoportail.wallonie.be/catalogue/e08bd187-c2e0-4c55-b262-226e0b27e486.html
- Réseaux d'itinéraires balisés reconnus par le CGT (quand disponible)
- Aire de pique-nique / BBQ (données DNF)
- Les Forêts d’Ardenne - Massifs forestiers http://geoportail.wallonie.be/catalogue/6babbdbf-a3d6-4c1d-96af-c43264a35ae3.html



17.De même, avez-vous déjà une idée d'autres données (hors Géoportail de la Wallonie) que vous pourriez également exploiter ?

- Fond carto IGN Topo
- Fond carto basés les données OpenStreetMap
- Réseaux de chemins et sentiers basés sur OpenStreetMap



18.Comment avez-vous entendu parler de ce Geochallenge ?
Géoportail de la Wallonie
Médias
Facebook
Twitter
Linkedin
Autre

19.Je confirme avoir pris connaisssance du Règlement du Geochallenge et je confirme la candidature




## Nouvelles questions après avoir déposé

### Quelles sont les problématiques soulevées par le défi choisi qui trouveront réponses grâce à la solution proposée ?

De par notre expérience et des consultations avec des acteurs de terrain, nous avons identifié plusieurs problématiques pour l'organisation de manifestations sportives avec parcours:

- peu ou difficulté de trouver des informations officielles de la Wallonie (réseaux RAVEL, réseaux balisés, chasses, ...) sur des outils existants permettant faire des parcours de randonnées. Par exemple, il existe une multitude de plateformes web et mobile permettant de préparer un itinéraire (AllTrails, GraphHopper, Strava, ViewRanger, GoogleMaps, ...), mais aucune ne permet d'accéder à des informations officielles de la Wallonie. 
- lourdeur administrative pour soumettre le dossier de l'événement
- absence d'informations sur des zones de restrictions

Ces contraintes s'appliquent aux organisateurs de manifestations sportives, mais aussi en partie au grand public. 

### Quels seront les principaux utilisateurs ou/et bénéficiaires de la solution ?
- Organisateurs de manifestations sportives (comité de village/quartier, associations, clubs sportifs)
- Services DNF locaux
- Administrations communales
- Grand public (dans une extension possible)

### Quelles seront les ressources technologiques et informatiques mise en œuvre pour le développement de votre solution ?

Nous prévoyons d'utiliser la pile logicielle suivante:

* Base de données PostgreSQL/PostGIS
* Back-end Python-Django avec extensions Django requises (GeoDjango, ...)
* Développement front-end Javascript ou Typescript avec OpenLayers

Néanmoins, cette pile logicielle pourrait être revue lors d'une première phase d'analyse. Nous développerons en utilisant des environnements de développement réplicables grâce à Docker et avec l'aide d'un système de version basé sur git.


### Pouvez vous précisez les principes envisagés pour donner une dimension spatiale et géomatique à votre projet ?

La solution proposée sera un portail cartographique avec une carte interactive permettant aux utilisateurs de créer des itinéraires. Il est probable que les utilisateurs doivent s'enregistrer. L'utilisateur pourra exporter ses tracés au format GPX pour les utiliser dans d'autres applications. En option, il pourra imprimer une carte à haute résolution de ses parcours. 

### Quelles données du Géoportail de la Wallonie seront utilisées et sous quelle forme (téléchargement, webservices, ...) ?

Une série de données du Géoportail seront utilisés en tant que webservices:
* Des WMS pour visualiser: Orthophotoplans, zones de contraintes, aires de pique-nique, réseaux ravels, ...
* Des WFS pour détecter automatiquement les contraintes et les zones administratives concernés par les parcours (Limites administratives du DNF, limites communales, zone de conservations de la nature, zones de chasse, ... )


### Quelles autres données seront créées ou utilisées et quelles en sont les sources ? (données ouvertes ou pas, origine, ...)

D'autres données seront nécessaires:
* Fonds cartographiques IGN (pour la visualisation): si possible
* Fonds cartographique basé sur OpenStreetMap, que ce soit les tuiles standards (par exemple celles servies par le géoportail) ou un style particulier (OpenTopoMap, OpenArdenneMap, ...)
* Réseaux de chemins et sentiers provenant d'OpenStreetMap: au format vectoriel pour le calcul d'itinéraire

Enfin, l'application mènera à la création de plusieurs itinéraires par les utilisateurs. 

### Selon vous, forces et faiblesses

Nos forces:

- Expériences pratiques de l'organisation d'événements sportifs: création des circuits, dépôt de dossier aux autorités communales et DNF et balisage sur le terrain

- Contacts avec les utilisateurs: Pour préparer cette proposition, nous l'avons préalablement soumise à des acteurs de terrains: 1) Manu Dupuis, agent DNF à Habay-la-Neuve, 2) Olivier Barthelemy, échevin de la nature, de la forêt et du tourisme de Habay-la-Neuve, 3) Catherine Colson, ingénieure DNF à la Direction de Mons. Ces avis nous ont permis d'affiner la proposition en recueillant les avis d'acteurs de terrain (DNF et autorité communale). Ces personnes-ressources pourront nous aider à affiner la proposition durant le stade de développement.

- Expériences de mise en place de portails cartographiques (cfr https://www.champs-libres.coop/page/webgis/). Nous maitrisons la pile logicielle nécessaire pour développer et maintenir une application web cartographique: serveur, moteur de rendu cartographique (GeoServer, MapServer), client web cartographique (OpenLayers, LeafletJS), langage de programmation web backend et frontend (PHP-Symfony, Python-Django, Typescript, Javascript)

- Bonne connaissance de services de calculs d'itinéraires (GraphHopper, OpenRouteService)

- Très bonne connaissance de l'extraction, le traitement et l'utilisation des réseaux de voiries disponibles sur OpenStreetMap en Belgique.

Nos faiblesses:

- Peu de connaissance des services du SPW concernés (mis à part des services DNF sur le terrain): CGT, DNF, ...
- Le besoin de développement requis pour ce projet pourrait dépasser notre temps disponible
- Peu de compétence graphique dans notre équipe (hormis de cartographie)


### Dans le même ordre d’idée, quelles sont les forces et faiblesses liées au développement et au déploiement de votre solution ?

Forces:
   - Besoin réel venant du terrain et exprimé lors d'une réunion de préparation (à distance) avec l'équipe du géoportail
   - Besoin bien identifié et circonscrit. Les limites d'utilisation, le public (organisateurs de manifestations sportives) et les bénéficiaires sont bien identifiés (communes, DNF).
   - Disponibilité en données élevées: les données SPW et OpenStreetMap sont d'ores et déjà suffisantes pour développer l'application, même si d'autres données seraient les bienvenues (zones de chasses avec dates de battues, réseaux balisés CGT, ...)
   - La solution se démarque des applications concurrentes par la fourniture de données officielles de la Wallonie. 

Faiblesses:
  - Risque que l'application ne soit pas utilisée par le public concerné (organisateurs de manifestations sportives);
  - difficulté éventuelle d'utilisation de l'outil. Certains publics ne sont pas assez familiers avec l'informatique pour élaborer un itinéraire sur une carte interactive. Les cartes papiers sont alors une alternative préférée. 



### Si oui, merci de préciser votre expérience en quelques lignes :


Nous réalisons habituellement des portails cartographiques pour des administrations (principalement Région Bruxelles-Capitale) (cfr https://www.champs-libres.coop/page/webgis/). Nous faisons également du traitement de données géographiques (par exemple, https://www.champs-libres.coop/portfolio/post/lidaxes/), du support et du développement QGIS (https://www.champs-libres.coop/page/qgis/) et de la cartographie (Création de cartes de promenades pour Natagora, Carte de l'entité de Brugelette). 


### Info complémentaires

Nous avons justement préparé 2 petites "users stories" qui ne rentraient pas dans les autres cases du formulaire!

Jean-Pierre et sa marche Adeps
------------------------------

Nouvellement élu président de son club de belote, Jean-Pierre est chargé d'organiser une marche Adeps dans l'espoir de récolter des fonds. Après avoir décidé du tracé sur des cartes IGN au cours de longue soirées et après repérage sur le terrain, il apprend qu'il doit demander des autorisations auprès des 2 communes traversées et de la DNF afin de valider son tracé. Jean-Pierre a d'ailleurs des doutes quant à savoir si on peut traverser les terres du baron de Hérode. Lors d'un de ses déplacement à Namur, Jean-Pierre fait des photocopies A1 des cartes IGN concernées puis trace le trajet à l'aide d'un fluo. Après plusieurs allers-retours entre la commune et la DNF et de multiples annotations sur la carte (car il a fallu faire des détours pour éviter les chemins traversant la propriété de de Hérode, ainsi que toute une portion d'un bois qui sera justement en chasse ce jour-là), il doit refaire des photocopies à Namur afin de soumettre sa carte finale. Bien que retraité, Jean-Pierre se dit que soumettre un tracé pour un tel événement est une démarche qui prend pas mal de temps. C'est aussi l'avis de Ludovic, le stagiaire DNF qui s'est empressé d'encoder dans Google Earth le tracé reçu à trois reprises avant d'entendre à chaque fois l'ingénieure lui dire qu'il fallait le changer.


Sarah et son Run&Bike
------------------------

Trailleuse chevronnée, jamais à court d'idée, Sarah décide d'organiser avec son club de running un événement sportif novateur: un Run & Bike d'orientation en Crocs®. Adepte des nouvelles technologies, elle n'a aucun mal à planifier et créer les 7 parcours au format GPX à l'aide de l'application Strava. Par contre, elle tombe des nues lorsqu'elle apprend qu'il faut faire des demandes officielles à toutes les communes traversées (4!) et à la DNF, avec des délais variables et même des supports différents: telle commune exige une copie des cartes papier tandis que les autres communes se satisfont d'une version électronique. Deuxième déconvenue, elle prépare ses 7 cartes avec un outil trouvé en ligne sur un forum: InkAtlas, mais Manu, l'agent DNF qui reçoit les cartes, les lui renvoie car la résolution de la carte ne permet pas de voir si un des tracés passe dans la zone de quiétude de son triage. Habitué des cartes IGN, Manu préférerait d'ailleurs recevoir ces cartes avec un fond IGN. Enfin, dernière déconvenue: lors du balisage la veille de l'événement, Sarah et son équipe s'aperçoivent qu'un balisage d'une marche Adeps pour un club de belote organisée le même jour chevauche ses itinéraires sur plusieurs tronçons.

