\newpage 

## Illustrations de l'application

À ce stade, l'application est une application en une seule page, optimisée pour un affichage en écran moyen ou grand. 


### Visualisation de l'itinéraire

La carte prend la majeur partie de l'espace de l'application. À gauche, un panneau permet d'effectuer les actions sur la carte: téléverser un itinéraire existant au format GPX, ou créer un nouvel itinéraire. Pour créer un nouvel itinéraire, l'utilisateur peut encoder des noms de lieu dans les champs à gauche, et/ou créer des points de passage directement en cliquant sur la carte. Il peut ensuite ajouter ou supprimer des points de passages. 

Dès qu'il y a plus de 2 points de passage, un itinéraire est automatiquement créé sur la carte et le profil altimétrique est affiché. 

\begin{figure}[htbp]
  \caption{Illustration de la plateforme - création d'un itinéraire}
  \centering
    \includegraphics[width=\textwidth]{./img/illu1.png}
\end{figure}

### Procédures administratives

Lorsque l'itinéraire est terminé, l'utilisateur peut cliquer sur un bouton pour soumettre son itinéraire et calculer les intersections avec les territoires des cantonnements DNF et des communes. Les procédures administratives sont ensuite affichées à droite de la carte. 


\begin{figure}
  \caption{Illustration de la plateforme - procédures administratives}
  \centering
    \includegraphics[width=\textwidth]{./img/illu2.png}
\end{figure}