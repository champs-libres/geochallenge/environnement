\newpage


# Documentation générale
## Public cible {#sec:public}

Nous identifions 2 types de publics visés par cette application: les organisateurs d'événements et le personnel des administrations concernées par cet événement.

### L'organisateur

L'**organisateur·rice de l'événement** est la personne qui encode et/ou crée des itinéraires sur l'application en vue de les communiquer aux administrations concernées. 

Il a des compétences en cartographie très variables. Il utilise souvent des outils disponibles sur le web pour créer ses parcours ou bien des cartes papiers (IGN). Il veut connaitre facilement et le plus tôt possible les différentes démarches à effectuer et disposer d'un maximum d'information préalablement à sa demande (quant aux contraintes d'exclusion). Il veut également savoir quels sont les itinéraires des événements sportifs déjà prévus au même moment que son événement. Il est éventuellement intéressé par avoir un fichier à imprimer à haute résolution de la carte des parcours de son événement, afin de l'afficher le jour de l'événement, ainsi que d'une webmap et des fichiers GPX à intégrer dans son site web ou à distribuer sur les réseaux sociaux.

Parfois, la personne qui utilise l'application ne fait pas partie au sens strict des organisateurs de l'événement mais est une personne qui vient en support aux organisateurs. 


### Les administrations

Le **personnel des administrations** n'est pas un utilisateur direct de la plateforme mais il reçoit les cartes des itinéraires qui ont été créées par l'application. Néanmoins, dans un développement ultérieur de l'application, il pourrait avoir accès dans l'application à tous les événements sportifs dans sa zone d'intérêt via un accès sécurisé. Les administrations pourraient également mettre à jour les zones d'exclusion et des données temporaires pertinentes (fermeture execeptionnelle de forêts pour cause de battues, ...)

Les administrations identifiées sont au moins les communes, les cantonnements du Département de la Nature et des Forêts (DNF) et le SPW Mobilité & Infrastructure. 

- Le cantonnement DNF local est l'autorité principale quant à l'autorisation des choix des tracés. Celui-ci peut exclure des parties des parcours proposés en fonction de contraintes d'exclusion que l'organisateur pourrait connaitre mais qui ne sont pas facilement accessibles (chemins privés, zones de quiétude, fermeture temporaire pour cause de chasse, ...) ou même que l'organisateur ne peut connaitre à l'avance (zone de nidification d'espèces rares, ...). Ces informations peuvent varier d'une année à l'autre. Il doit également veiller à la coexistence des multiples activités ayant lieu en forêt: vie sauvage, chasse, manifestations sportives, exploitation forestière, etc. Suite à une demande introduite par notre coach (Marie Wenin) auprès de cantonnements DNF, nous savons que le format des parcours souhaité est variable selon les cantonnements (cfr Annexe 2)

- La ou les commune(s) concernée(s) par la manifestation a (ont) besoin des informations des tracés afin de compléter le dossier de sécurité de l'événement. En cas de problème, les services de secours doivent être en mesure de savoir où sont les participants de l'événement. La commune peut en outre désirer promouvoir son réseau d'itinéraires balisés aux organisateurs, plutôt que de faire circuler les participants de l'événement sur des chemins moins entretenus.

- Les administrations en charge des routes, comme le SPW Mobilité & Infrastructures pour les routes régionales ont éventuellement besoin de connaitre les traversées de route par les itinéraires. 


### Autres utilisateurs

Avec des développements supplémentaires, l'application pourrait être destinée au grand public, comme outil de planification de randonnée. Néanmoins, il existe un nombre considérable d'applications similaires pour le grand public (cfr Annexe 2, page \pageref{sec:review}). un moyen de différencier le produit serait de proposer des données officielles de l'administration introuvables ailleurs.  