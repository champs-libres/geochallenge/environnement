\newpage


# Notre équipe

## Champs-Libres

Champs-Libres est une société coopérative de droit belge, à finalité sociale. Elle s'inscrit dans le
développement de l'économie sociale et solidaire, par la fourniture de services lié au logiciel libre aux
associations, entreprises d'économie sociale et aux pouvoirs publics. Champs-Libres a été créée en
2013 et emploie actuellement 4 travailleurs temps-plein.

Nous sommes spécialisés dans l'utilisation et le développement de **logiciels open-source dans la domaine de la géomatique**, que ce soit pour construire des applications métiers ou des portails géographiques. Nous avons notamment une expertise approfondie dans les logiciels suivants:

* QGIS,
* PostGIS,
* GDAL,
* Django et GeoDjango,
* les serveurs de tuiles (Mapnik, renderd, mod_tile, cartoCSS, ...).

Nous avons une grande expérience pratique dans des **projets autour d'OpenStreetMap** et de sa communauté. En tant que membres de la communauté OpenStreetMap (OSM) belge, nous promouvons OSM en organisant des rencontres de contributeurs et des formations, et en aidant à déployer des serveurs de tuiles mutualisés.

Site web: [www.champs-libres.coop](https://www.champs-libres.coop)


## La genèse du projet

Quand nous ne sommes pas penchés sur nos écrans, il nous arrive de sortir à l'air libre. Nous avons en effet une expérience pratique d'organisations de manifestations sportives où _orgarando_ serait tout à fait opportun. Par ces expériences, nous avons connu la problématique de création des circuits, du dépôt de dossier aux autorités communales et au cantonnement Département Nature et Forêts (DNF) ainsi que le balisage du tracé sur le terrain. 

En outre, pour préparer ce projet, nous avons contacté différents utilisateurs potentiels côté "administrations": 

1) Manu Dupuis, agent DNF à Habay-la-Neuve, 
2) Olivier Barthelemy, échevin de la nature, de la forêt et du tourisme de Habay-la-Neuve, 
3) Catherine Colson, ingénieure DNF à la Direction de Mons. 

Ces avis nous ont permis d'affiner la proposition en recueillant les avis d'acteurs de terrain.