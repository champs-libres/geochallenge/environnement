import io
import unittest
from events.serializer import EventSerializer
from events.models import Event
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer

# Create your tests here.
class EventSerializerTest(unittest.TestCase):
    def test_ok(self):
        self.assertTrue(True)

    def test_serialize(self):
        content = JSONRenderer().render(self.EVENT_GEOM)
        stream = io.BytesIO(content)
        data = JSONParser().parse(stream)
        serializer = EventSerializer(data=data)
        serializer.is_valid()
        event = Event(**serializer.validated_data)
        self.assertTrue(isinstance(event, Event))
        self.assertTrue(event.itinerary is not None)
        self.assertTrue(event.itinerary.srid == 31370)
        self.assertTrue(event.itinerary[0][0] < 500000)


    EVENT_GEOM = {
        "id": 2,
        "itinerary": {
            "type": "LineString",
            "coordinates": [
                [
                    581485.9467022158,
                    6493473.3537829295
                ],
                [
                    566261.8146489781,
                    6505828.1652901815
                ],
                [
                    583388.9032411651,
                    6491880.11508362
                ]
            ]
        },
        "createdAt": "2020-09-02",
        "eventDateStart": "2020-09-10",
        "eventDateEnd": "2020-09-04",
        "eventType": "None"
    }
