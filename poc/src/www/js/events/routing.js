import { addRoutingInput, doRouting } from "../routing/routing";
import { getWayPoints } from "../map/layers";
import { saveItinerary } from "./post";


export const routingFormEvents = () => {
    /**
    * Initialize the routing form and bind events
    */
    addRoutingInput(1);
    addRoutingInput(2);
    document.getElementById('geocodeInput-1').focus();

    const wayPointButton = document.getElementById('wayPointButton');
    wayPointButton.addEventListener('click', (_e) => {
        const routingInputNumber = document.querySelectorAll('form.geocode input').length;
        addRoutingInput(routingInputNumber + 1);
    });

    const routingForm = document.getElementById('routing');
    const wayPoints = getWayPoints();
    routingForm.addEventListener('change', (_e) => {
        if (wayPoints.length > 1) {
            doRouting(wayPoints);
        }
    });

    const saveButton = document.getElementById('saveButton');
    saveButton.addEventListener('click', (_e) => {
        saveItinerary(_e);
    });
}