\newpage 

## Fonctionnalités et état d'avancement

### État d'avancement

Le tableau suivant présente une liste des fonctionnalités qui ont été développées pour le prototype présenté au 24/09/2020, avec une indication du temps comptabilisé pour le développement. Il faut noter que les fonctionnalités de création d'itinéraire, de géocodage, d'intersection et d'affichage des procédures administratives ont été développées telles qu'elles devraient être dans le produit final: par exemple, il n'y a pas de données ou de fonctions "fake" dans ces fonctionnalités. 


\begin{table}[hbtp]
\small
\begin{tabular}{|p{11cm}|p{1.1cm}|}
\hline
\textbf{Fonctionalité} & \textbf{heures} \\
 & \textbf{dév.} \\
\hline
App bootstrap & 12 \\ 
Back: Entités & 4 \\ 
Back: POST un trajet & 6 \\ 
Back: Intersection entre trajet et couches d’intersections & 6 \\ 
Back: GET procédures & 4 \\ 
Front: Structure de la page “webmap” & 4 \\ 
Front: Formulaire de routage (y compris géocodage) & 12 \\ 
Front: Interaction sur la carte (déplacement du tracé, ajout de points) & 8 \\ 
Front: Affichage des procédures & 2 \\ 
Front: Fonds de carte & 2 \\ 
Front: Profil altimétrique & 8 \\ 
Intégration d’un design & 8 \\
\hline
TOTAL &	76 \\
\hline
\end{tabular}
\caption{Fonctionalités développées au stade du prototype}
\end{table}

### Fonctionnalités à développer pour un produit minimum viable
	
Pour un développement d'un produit minimum viable (MVP), nous esquissons ci-dessous une liste des fonctionnalités qu'il reste à développer. Cette liste n'est pas exhaustive. Nous estimons qu'un MVP nécessiterait le double d'heures de développement que celles engagées pour le développement du prototype. 


* Back: **Intersection avec couches des communes**: Seule la couche des cantonnements DNF est utilisée dans le prototype pour définir une liste des procédures. La couche des communes est également nécessaire pour ajouter les procédures communales. 

* Back: **Intersection avec réseau routier**: L'intersection du trajet et la couche du réseau routier régional est nécessaire pour afficher les procédures quant à la traversée de routes régionales.

* Front/back: **Couches d'exclusion**: Servir des couches d’exclusion et empêcher automatiquement le calcul d’itinéraire de passer par les zones d’exclusions.

* **Meilleure définition des procédures**: Améliorer le libellé des procédures de contact avec le DNF et les communes. Personnaliser l'information suivant les cantonnements et les communes. Ce point nécessite des discussions avec les services concernés. 

* Front/back: **Couches du géoportail**: Ajouter des couches du géoportail (communes, cantonnement DNF, ravel, promenades, …) qui donnent de l'information supplémentaire à l'organisateur. 

* Front: **Profil d’itinéraire**: Ajouter un sélecteur de profil de la randonnée (marcheur, VTTiste, e-bike, cyclo, …) pour améliorer le calcul d'itinéraire.

* Front: **gestion des erreurs JS, affichage de message**: Améliorer la capture des erreurs dans le code Javascript et afficher des messages "toasts" à l'utilisateur au besoin.	

* Front: **Import/Export GPX**: Permettre l'import d'une trace GPX dans l'application, ainsi que l'export au format GPX d'un itinéraire créé dans l'application.

* Front/back: **Partage avec lien rapide**: Permettre le partage de la carte avec un lien rapide que l'utilisateur peut envoyer aux administrations

* Front: **Impression**: Permettre l'impression de la carte avec une mise page cartographique (légende, échelle, grille, orientation). À ce stade, cette impression ne serait possible qu'en format JPEG ou PNG avec une résolution moyenne.

* **Intégration d’un design**: Améliorer le design et intégrer les éléments ajoutés au stade du MVC. 
	
	
	
### Autres fonctionnalités / vision
	
Nous présentons ici une liste de ce que l'outil pourrait devenir avec davantage de moyens. 

* Accès admin pour les administrations: Un accès sécurisé et intégré aux systèmes d'information des administrations permet de gérer les demandes d'autorisation d'événements sportifs, de contacter les organisateurs via la plateforme, de collecter des statistiques sur les événements, etc. Une messagerie met en lien directement les organisateurs et les fonctionnaires en charge du dossier d'autorisation. 

* Pemettre aux administrations de personnaliser leurs procédures: Via l'accès admin précité, les administrations peuvent personnaliser la procédure pour l'envoi des cartes et/ou des itinéraires: cartes web, cartes papier, par email ou formulaire, etc. 

* Créer des comptes utilisateur complets pour les organisateurs: Création d'un espace privé pour les organisateurs au sein de l'application, avec authentification. 

* Intégration de l'application dans les formulaires en ligne de la Wallonie (guichet SPW "monespace.wallonie.be"), pour les organisateurs.

* Interaction entre profil altimétrique et tracé sur la carte: Un déplacement de la souris sur le tracé / profil altimétrique indique la position du curseur sur le profil altimétrique / tracé. 

* Améliorer la possibilité de tracer des boucles ("roundtrip"): la création d'un tracé en boucle est facilitée sur la carte.

* Impression papier haute-définition: Une fonctionnalité permet d'imprimer la carte de l'événement en haute résolution au format PDF, avec choix des fonds de plan. Cette fonctionnalité est destinée aux organisateurs de l'événement.

* Formats d'échanges de données géographiques: Les utilisateurs peuvent importer et exporter des tracés sous différents formats (KML, geoJSON, shp, gpkg, ...). Dans le MVP, seul le format GPX sera supporté.

* Créer une couche géoportail avec tous les événements autorisés.	

* Déploiement de la solution, hébergement et/ou intégration dans l'architecture des systèmes d'informations de la Wallonie: À terme, la solution développée sera une application web qui devra être maintenue par une autorité qu'il reste à identifier. Celle-ci devra pouvoir administrer l'application afin de la faire évoluer avec des données variables dans le temps.
