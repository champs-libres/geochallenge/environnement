const addEventsLeftPanel = () => {
    document.getElementById('leftPanelCollapseBtn').addEventListener('click', e => {
        const panel = document.getElementById('leftPanel');
        const icon = document.getElementById('leftPanelCollapseBtn').querySelectorAll('i')[0]
        if (!(panel.classList.contains('collapsed'))) {
            panel.classList.add('collapsed');
            icon.classList.remove('fa-chevron-left');
            icon.classList.add('fa-chevron-right');
        } else {
            panel.classList.remove('collapsed');
            icon.classList.remove('fa-chevron-right');
            icon.classList.add('fa-chevron-left');
        }
    });
};

const addEventsRightPanel = () => {

    const rightPanelContent = document.getElementById('rightPanelContent');
    const rightPanelHeader = document.createElement('div');
    rightPanelHeader.classList.add('header');
    rightPanelHeader.id = 'rightPanelHeader';
    rightPanelHeader.innerText = 'Votre itinéraire croise les cantonnements suivants. Vous devez contacter les cantonnements avec les informations ci-dessous pour obtenir les autorisations';
    rightPanelContent.appendChild(rightPanelHeader);
    

    document.getElementById('rightPanelCollapseBtn').addEventListener('click', e => {
        const panel = document.getElementById('rightPanel');
        const icon = document.getElementById('rightPanelCollapseBtn').querySelectorAll('i')[0]
        if (!(panel.classList.contains('collapsed'))) {
            panel.classList.add('collapsed');
            icon.classList.remove('fa-chevron-left');
            icon.classList.add('fa-chevron-right');
        } else {
            panel.classList.remove('collapsed');
            icon.classList.remove('fa-chevron-right');
            icon.classList.add('fa-chevron-left');
        }
    });
};

export const openRightPanel = () => {
    const panel = document.getElementById('rightPanel');
    panel.classList.remove('collapsed');
};

export const openBottomPanel = () => {
    const panel = document.getElementById('bottomPanel');
    panel.classList.remove('collapsed');
};

export const closeBottomPanel = () => {
    const panel = document.getElementById('bottomPanel');
    panel.classList.add('collapsed');
};

const addEventsBottomPanel = () => 
    document.getElementById('bottomPanelCloseBtn').addEventListener('click', closeBottomPanel);


export const addEventsPanels = () => {
    addEventsLeftPanel();
    addEventsRightPanel();
    addEventsBottomPanel();
}