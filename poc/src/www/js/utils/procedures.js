import { openRightPanel } from "../events/panels";

export const getProcedure = (id) => {
    
    const url = `http://localhost:18000/api/events/${id}/procedures/?format=json`;
    fetch(url, {
        method: 'GET',
    })
    .then(response => response.json())
    .then(data => {
        displayProcedure(data);
    }).catch(error => {
        console.log(error);
    })

};


export const displayProcedure = (data) => {

    const rightPanelContent = document.getElementById('rightPanelContent');
 
    const displayProcedureItem = (div, dataIndex, item) => { 
        const divItem = document.createElement('div');
        divItem.classList.add(`procedureItem__${item}`);
        divItem.innerHTML = dataIndex[item];
        div.appendChild(divItem);
    };

    for (let i = 0; i < data.length; i++) {
        let div = document.createElement('div');
        div.classList.add('procedureItem');

        let dataIndex = data[i];

        displayProcedureItem(div, dataIndex, 'title', );
        displayProcedureItem(div, dataIndex, 'description');
        displayProcedureItem(div, dataIndex, 'contact');
      
        const divItemPhone = document.createElement('div');
        divItemPhone.classList.add(`procedureItem__phone`);
        const phone = dataIndex['tel']
        divItemPhone.innerHTML = `<i class="fas fa-phone-alt"></i>${phone}`;
        div.appendChild(divItemPhone);

        const divItemMail = document.createElement('div');
        divItemMail.classList.add(`procedureItem__email`);
        const email = dataIndex['email']
        divItemMail.innerHTML = `<i class="fas fa-envelope"></i><a href="mailto:${email}" >${email}</a>`;
        div.appendChild(divItemMail);
   
        rightPanelContent.appendChild(div);
    }

    openRightPanel();
};