\newpage

## Scenarii d'utilisation

### Flux général de l'information

Un schéma présentant le processus général simplifié de l'application est présenté dans la figure suivante. Les tracés, soit fournis par un fichier GPX, soit par dessin sur la carte, sont intersectés avec des couches administratives. Le résultat de cette intersection est renvoyé vers l'application sous forme de texte: ce sont les procédures administratives à suivre pour obtenir les autorisations des entités traversées. 

\begin{figure}[hbtp]
  \caption{Schéma du processus général de l'application}
  \centering
    \includegraphics[width=\textwidth]{./img/scenarii-v2.pdf}
\end{figure}

Les _tracés_ sont une couche géographique sous forme de ligne représentant le ou les tracés de l'événement de l'organisateur. 

Les _couches administratives_ sont des couches géographiques de données qui seront intersectées avec le tracé pour identifier les procédures administratives propres à ce tracé. Cela doit permettre à l'organisateur, selon le découpage administratif du territoire (communes, cantonnement DNF), de visualiser les procédures administratives qu'il doit engager. 

Les _procédures administratives_ consistent en des instructions sur comment envoyer les cartes avec circuits à l'administration. Cela peut être envoyer une carte imprimée sur du papier par voie postale, une carte au format pdf ou jpeg de l'application par e-mail, ou envoyer des fichiers GPX des circuits par e-mail, ou encore prendre contact par téléphone pour connaitre la marche à suivre. A noter qu'un envoi automatique des données de tracés aux administrations nous parait peu souhaitable afin d'établir un contact entre l'organisateur et les administrations, et pour que l'organisateur puisse controler ce qu'il envoie. 

Comme décrit dans la section \ref{sec:public}, nous distinguons essentiellement 2 types d'utilisateurs de la plateforme: l'organisateur et le personnel des administrations concernées. Des mises en situation ont été décrites en guise d'introduction à la page \pageref{sec:resume}. 

### Utilisation de l'application par l'organisateur de l'événement sportif

#### 1) L'organisateur possède le(s) circuit(s) sour forme numérique.

Pour beaucoup d'événenements, les organisateurs disposent déjà de fichiers reprenant les circuits sous forme digitale, habituellement au format GPX. Ces fichiers proviennent d'autres applications web, mobiles ou d'instruments de randonnées (GPS de randonnée, montre GPS, ...). L'organisateur doit donc être en mesure de pouvoir simplement téléverser cette trace afin de voir quels découpages administratifs sont concernés par son ou ses tracés, et quelles sont les procédures sous-jacentes. Les étapes sont:

1) Téléverser la trace dans l'application (remplir un champ avec le fichier et cliquer sur un bouton pour soumettre)
2) Visualiser le circuit sur la carte
3) Visualiser les procédures administratives

#### 2) L'organisateur ne possède pas le(s) circuit(s) sour forme numérique.

Dans le cas où l'organisateur ne possède pas les circuits sous forme numérique, la plateforme permet de numériser le ou les circuits. Les étapes sont:

1) Tracer un circuit en:

- cliquant sur des endroits de la carte: A chaque click, un point de passage (way point) apparait sur la carte. 
- ajoutant des lieux dans le formulaire à gauche de la carte. Lorsque le lieu est reconnu par géocodage, un point de passage apparait sur la carte.
- supprimer/ajouter des points de passage interactivement sur la carte ou dans le formulaire pour ajuster le circuit.

Dès qu'il y au moins 2 points de passage, un tracé est automatiquement calculé. 

2) Quand le circuit sur la carte est celui souhaité, le soumettre à l'application pour voir les procédures (cliquer sur un bouton pour soumettre);

3) Visualiser les procédures administratives.


#### 3) L'organisateur a déjà utilisé l'application une année précédente (pas encore implémenté)

Il est courant que les événements se reproduisent d'une année à l'autre. Dans ce cas, l'organisateur doit pouvoir retrouver le ou les tracés de l'événement des années précédentes et de le soummettre pour visualiser les procédures administratives. Les étapes sont:

1) Aller dans un espace personnel de gestion des événements;
2) Sélectionner le circuit;
3) Visualiser les procédures administratives (qui ont été éventuellement actualisées)


### Utilisation par le personnel des administrations (pas encore implémenté)

Actuellement, les procédures administratives consistent en des instructions sur comment envoyer les cartes avec circuits à l'administration. 

Dans un second temps d'implémentation, l'application pourrait servir au personnel des administrations pour directement visualiser les circuits et gérer les demandes d'autorisation au travers de l'application. Un accès sécurisé permettrait alors d'avoir accès à toutes les demandes de circuits sur un territoire donné. 

Au final, l'outil pourrait servir afin d'évaluer la fréquentation globale sur une année de portions du territoire (massifs forestiers, circuits balisés, etc.).

