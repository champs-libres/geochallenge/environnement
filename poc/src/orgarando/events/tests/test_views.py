from django.test import TestCase, Client
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from events.models import Event
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType

class TestEventView(TestCase):

    def setUp(self):
        self._user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

        content_type = ContentType.objects.get_for_model(Event)
        all_permissions = Permission.objects.filter(content_type=content_type)
        for p in all_permissions:
            self._user.user_permissions.add(p)
       
    def tearDown(self):
        self._user.delete()

    def test_create(self):
        c = APIClient()
        tr = c.force_login(self._user)
        response = c.post('/api/events/', data=self._EVENT, format='json')

        self.assertEqual(201, response.status_code)

    def test_list(self):
        c = APIClient()
        response = c.get('/api/events/', format='json') 

        self.assertEqual(200, response.status_code)


    _EVENT = {
        "itinerary": {
            "type": "LineString",
            "coordinates": [
                [
                    581485.9467022158,
                    6493473.3537829295
                ],
                [
                    566261.8146489781,
                    6505828.1652901815
                ],
                [
                    531704.9702845849,
                    6510129.748180664
                ],
                [
                    539648.4409634559,
                    6492516.644188863
                ],
                [
                    576595.6312322053,
                    6476085.817739626
                ],
                [
                    586797.8752351875,
                    6461071.200124324
                ],
                [
                    605955.3471000366,
                    6482577.788534516
                ],
                [
                    598386.3466847492,
                    6499280.851118542
                ],
                [
                    583388.9032411651,
                    6491880.11508362
                ]
            ]
        },
        "createdAt": "2020-09-02",
        "eventDateStart": "2020-09-10",
        "eventDateEnd": "2020-09-04",
        "eventType": "None"
    }
