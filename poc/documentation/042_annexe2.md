\newpage

## Annexe 2: Review des applications de planifications de randonnées {#sec:annexe2}

Cette section répertorie les moyens disponible dans d'autres applications pour tracer un itinéraire dans une carte interactive.

**1) cycle.travel**

Exemple: de Spa au Signal de Botrange: <https://cycle.travel/map?from=spa&to=Rue%20de%20Botrange&fromLL=&toLL=50.5014514,6.0940255>

![](img/cycle.travel.png)

- pas besoin d'inscription, mais possible
- Sur la homepage, pas de carte, mais un form pour entrer son point de départ et sa destination
- On peut directement aussi aller sur la carte, et cliquer une première fois pour un point de départ, et une deuxième fois pour un point de destination. 
- Une fois les informations rentrées, un itinéraire est automatiquement tracé entre les points
- un bouton "roundtrip" permet de faire une boucle, entre les points de départ et d'arrivée mais avec des tracés différents entre l'aller et le retour. 
- l'itinéraire est optimisé pour du vélo (suit des itinéraires cyclables en priorité, etc.)
- Pour modifier le tracé, on peut déplacer les points de départ et d'arrivée, et ajouter des points en cliquant sur le tracé et en déplacant la souris. Le tracé se met à jour en direct lors du déplacement du point. 
- Pour supprimer des points ajoutés, on clique dessus, un popup s'ouvre et on peut le supprimer
- Les points supplémentaires ne s'affichent pas dans l'espace à gauche, seulement les points de départ et d'arrivée


Autres infos:

- les infos du tracé (longueur, type de surface) s'affichent dans l'espace à gauche et se mettent à jour en temps réel
- il y a aussi des instructions de navigation
- par défaut, des balises kilométriques (ou marqueurs de distances) s'affichent le long du tracé
- on peut exporter en GPX, sauvegarder dans son espace personnel et créer une carte en pdf
- l'export est très bien fait avec 4 niveaux d'échelles à choisir. 


**2) OpenRouteService.org/maps**

Ex: <https://maps.openrouteservice.org/directions?n1=50.486375&n2=5.980061&n3=13&a=50.490204,5.867943,50.501702,6.092628&b=0&c=0&k1=en-US&k2=km>

![](img/maps.openrouteservice.png)

- pas d'inscription
- Carte sur la homepage avec un form pour entrer son point de départ et sa destination
- Autre mode de création, on fait un simple clic sur la carte pour créer un marqueur, puis un clic droit sur la carte permet de définir le marqueur comme point de départ ou de destination. 
- On peut créer un itinéraire en important un fichier gpx, geojson ou kml (même si cela n'a pas fonctionné dans mes essais)
- On peut ajouter des points intermédaires en cliquant sur le tracé et en déplaçant la souris. Le tracé ne se met pas à jour en direct ce qui peut amener à une mauvaise expérience utilisateur. 
- Lorsqu'on ajoute des points intermédiaires, ils se mettent dans l'espace à gauche dans le form entre les points de départ et d'arrivée
- Pour supprimer un point intermédiaire, on le supprime dans le form, pas sur la carte
- On peut aussi ajouter un point intermédiaire dans le form

Autres infos:

- Il y a plusieurs mode de calcul d'itinéraire: voiture, marche, randonnée, vélo normal, VTT, vélo de course, speedelec, engin lourds...
- Profil altimétrique disponible
- il existe une option roundtrip qui génére un tracé autour d'un seul point en fct d'une distance
- on peut mettre des marqueurs de distance.


**3) https://www.alltrails.com/explore/map/new**

![](img/alltrails.com.png)


- besoin d'inscription
- Carte avec invitation à rentrer un lieu, et uniquement un lieu, qui permet de zoomer sur une région 
- On crée un itinéraire en cliquant sur la carte un point de départ, puis un point d'arrivée. Le tracé se fait automatiquement entre les deux points par la suite. 
- On peut aussi créer un itinéraire en uploadant un fichier gpx (ou autres)
- On ne peut pas modifier la trace en mode "Routing". Pour ajouter des points, on passe en mode "Drawing" mais le suivi du réseau des chemins est annulé: on ne fait que dessiner sur la carte


Autres infos:

- Il y a 6 profils différents de randonnées
- Il y a un profil altimétrique


**4) komoot.fr/plan**

Ex: <https://www.komoot.fr/plan/@50.4875446,5.9829055,13z?p[0][name]=Spa&p[0][loc]=50.492083,5.862623&p[1][type]=poi&p[1][id]=67e>

![](img/komoot.fr.png)

- pas besoin d'inscription
- Carte avec invitation à rentrer un lieu de départ et de destination, mais aussi un lieu général.
- Ensuite, plusieurs itinéraires sont proposés suivant différents profils (rando, VTT, cyclo, ...)
- On peut aussi créer un itinéraire en cliquant sur la carte un point de départ, puis un point d'arrivée. Le tracé se fait automatiquement entre les deux points par la suite.
- On peut ajouter des points intermédaires en cliquant sur le tracé et en déplaçant la souris. Le tracé se met à jour en direct (mais un peu lentement).
- On peut aussi ajouter un point intermédiaire dans le form à gauche. 
- Pour supprimer des points ajoutés, on clique dessus, un popup s'ouvre et on peut le supprimer

Autres infos:

- Il y a un profil altimétrique
- Il y a l'info sur les types de chemins
- Il y a l'info sur les surfaces de chemins

**5) routeyou**

Ex: <https://www.routeyou.com/fr-be/route/planner/0/planificateur-d-itineraires-outdoor>

![](img/routeyou.png)

- pas besoin d'inscription
- Dans le planificateur d'itinéraire, rentrer un point de départ et d'arrivée
- Possibilité de faire un "roundtrip" à partir d'un seul lieu de départ. On ajuste la longueur en faisant glisser un slider de distance
- Différents types de profil (randonnées à pied, différents types de vélo, ...) sont disponibles
- Possibilité d'enregistrement de l'itinéraire, mais après création d'un compte.

Autres infos:

- Bonnes capacité d'impressions
- Larges capacités d'export dans différents formats: GPX, mais aussi vers plusieurs applications mobiles

**6) bikemap**

Ex: <https://www.bikemap.net/en/routeplanner/?waypoints=ovdsHk%60xb%40c%7B%40o%7Ck%40#/z12/50.487912,5.98185/basic>

![](img/bikemap.png)

- Besoin d'inscription (proposition avec son compte Facebook, Google ou Apple)
- Dans le planificateur d'itinéraire, rentrer un point de départ et d'arrivée, ou lse sélectionner sur la carte
- On peut ajouter des points intermédaires en cliquant sur le tracé et en déplaçant la souris. Le tracé ne se met pas à jour en direct ce qui peut amener à une mauvaise expérience utilisateur. 
- Lorsqu'on ajoute des points intermédiaires, ils se mettent dans l'espace à gauche dans le form entre les points de départ et d'arrivée
- On peut aussi ajouter des points intermédiaires en les ajoutant dans le form à gauche
- On peut supprimer des points en sélectionnant un outil et en cliquant sur les points
- On peut tracer aussi une route à la main sans passer par des chemins
- On peut aussi importer sa trace GPX ou KML

Autres infos:

- export en GPX et KML disponible
- profil altimétrique disponible
