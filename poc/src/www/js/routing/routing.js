import { Directions } from 'openrouteservice-js/npm_distribution';

import { updateItinerary, zoomToItineraryLayer, removeWayPoint, getWayPoints } from '../map/layers';
import { geocodeInput, geocodePoints } from './geocoding';
import { updateProfile } from '../utils/profile';


const apiKey = '5b3ce3597851110001cf6248d35391925fe04b8792859894153cde3c'; // TODO

const getProfile = () => {
    // TODO: getProfile from user input
    return 'cycling-regular';
}


export const doRouting = (wayPoints) => {
    /**
    * Do routing from an array of XY coordinates in EPSG:3857
    * @param {number} id
    */
    let orsDirections = new Directions({
        api_key: apiKey
    });

    orsDirections.calculate({
        coordinates: wayPoints,
        profile: getProfile(),
        extra_info: ["waytype", "steepness"],
        format: "geojson",
        api_version: 'v2',
    })
    .then( (json) => {
        updateItinerary(json);
        zoomToItineraryLayer();
        updateProfile();
    })
    .catch( (err) => {
        let response = JSON.stringify(err, null, "\t")
        console.error(response);
    });

}

export const addRoutingInput = (id, uid = 0) => {
    /**
    * Add a routing input to the form in the DOM
    * @param {number} id
    */
    const inputDiv = document.createElement('div');
    inputDiv.classList.add('geocode__input','form-inline');
    const inputLabel = document.createElement('label');
    const input = document.createElement('input');
    
    inputLabel.innerHTML = id;
    input.type = 'text';
    input.id = `geocodeInput-${id}`;
    input.classList.add('form-control');
    input.placeholder = 'Entrez un lieu';
    input.addEventListener('keyup', (e) => { //TODO debouncing keyup event? 
        geocodeInput(e.target.value, e.target.parentNode);
    });

    inputDiv.appendChild(inputLabel);
    inputDiv.appendChild(input);

    if (id > 2) {
        const deleteButton = document.createElement('button');
        deleteButton.type = 'button';
        deleteButton.classList.add('btn','btn-light', 'btn-sm');
        inputDiv.setAttribute('data-uid', uid);
        deleteButton.setAttribute('data-uid', uid);
        deleteButton.innerHTML = '<i class="fas fa-trash-alt"></i>'
        deleteButton.addEventListener('click', (_e) => { 
            onDeleteButtonClick(id, uid);
        });
        inputDiv.appendChild(deleteButton);
    }
    
    document.querySelector('form.geocode').appendChild(inputDiv);

    input.focus();
};


const onDeleteButtonClick = (id, uid) => {
    removeRoutingInput(id);
    removeWayPoint(uid);
    doRouting(getWayPoints());
};

const removeRoutingInput = (id) => { // TODO by uid? 
    const routingInput = document.getElementById(`geocodeInput-${id}`);
    routingInput.parentNode.remove();
};

export const addCoordinatesToInput = (coordinates, id) => {
    /**
    * Add some coordinates to an routing input field
    * @param {number[]} coordinates - XY coordinate array in EPSG:4326
    * @param {number} id - id of the geocode input field
    */
    const routingInput = document.getElementById(`geocodeInput-${id}`);
    routingInput.value = `${coordinates[0].toFixed(4)} - ${coordinates[1].toFixed(4)}`;
};

export const updateRoutingFormWithCoordinates = (coordinates, uid) => {
    const emptyFeature = {};
    const wayPointsLength = getWayPoints().length;

    if (wayPointsLength > 2){
        addRoutingInput(wayPointsLength, uid); // TODO probleme: le compteur des points ne devrait pas être décrémenté lors de la suppression d'un point 
    }
    addCoordinatesToInput(coordinates, wayPointsLength);
    geocodePoints.push(emptyFeature);
};
