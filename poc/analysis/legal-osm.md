Est-ce que les données d'itinéraires générées seront sous licence odbl? 
--------------------------------------------------------------------------

Cette page https://wiki.osmfoundation.org/wiki/Licence/Licence_and_Legal_FAQ#What_exactly_do_I_need_to_share.3F, semble inidiquer qu'une route digitalisée à partir de données OSM est un "produced work", qui peut être licence avec une autre licence que l'ODbL, et qui ne déclenche pas la condition de partage sous la même licence. "In ODbL parlance, this is known as a "Produced Work"."

Un autre document stipule que le résultat de géocodage n'est pas un extrait de la BD OSM: https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines/Geocoding_-_Guideline. Cele ne déclenche pas la condition de partage sous la même licence (It does not trigger the Share-alike obligations)


Je n'ai pas trouvé de sources disant explicitement que des traces obtenues à partir des données OSM en utilisant un outil de routing étaient un "produced work", donc j'ai fait la demande de confirmation ce matin. 

Pour information, j'ai souvent des demandes des administrations/entreprises par rapport à ces questions et souvent, il y a la crainte qu'OSM ne puisse pas être utilisé sous peine de devoir délivrer tout une base de données sous la licence oDbL. En lisant cette page https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines, il est écrit explicitement qu'on peut distribuer des cartes mélangeant des données OSM avec d'autres données sans devoir utiliser la licence oDbL, que ce soit en mélangeant des données de régions géogrpahiques différentes ou en mélangeant des couches. Il est stipulé également que les données OSM peuvent être utilisées en interne avec beaucoup de liberté: par exemple des producteurs de données comme le SPW ou l'IGN pourraient utiliser OSM comme outil de comparaison de leur données sans devoir changer la licence de leurs données : cfr https://wiki.osmfoundation.org/wiki/Licence/Licence_and_Legal_FAQ#Are_there_any_special_conditions_or_restrictions_for_commercial_or_academic_use.3F, "Share-Alike only applies if you distribute what you have done to outside people or organisations. You can do what you like at home, or in your school, organisation or company ... the following section does not apply to you."

Bon, en même temps, je ne suis pas juriste ;-)


# Question envoyée à la mailing list legal@osm.org:


Hello, 

I have a use case that I think is very common but I could not find on the osmfoundation.org/wiki/Licence/ pages any information about it:

Suppose I build a website with a map and a routing engine such as OpenRouteService that is using OSM data. The routing engine is used by users to generate routes data from a point A to a point B. These data can then be exported in GPX or geoJSON or any suitable format. Can these data (eg the GPX files) be shared under another licence that the oDbL? Does it trigger the Share-alike obligations?

Actual implementations of this issue already exist: https://maps.openrouteservice.org, cycle.travel, and many others. 

I found that geocoding outputs (see here https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines/Geocoding_-_Guideline) from OSM data can be shared under another licence than the oDbL and that it does NOT trigger the share-alike obligation. I guess this is the same in my case, but I need to be sure of that and I think this information could be added in the osmfoundation.org/wiki/Licence/ pages. 

Many thanks in advance!

Julien Minet, aka juminet



# Réponse: 

Dear Julien,

we would very likely this consider this a produced work, however please see  https://wiki.osmfoundation.org/wiki/Licence/Community_Guidelines/Produced_Work_-_Guideline  on using a Produced Work for extracting OSM data.

Kind regards

Simon Poole

-- 

OpenStreetMap Foundation
St John’s Innovation Centre
Cowley Road
Cambridge
CB4 0WS
United Kingdom
A company limited by guarantee, registered in England and Wales.
Registration No. 05912761. 