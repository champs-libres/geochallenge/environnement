import TileLayer from 'ol/layer/Tile';
import TileWMS from 'ol/source/TileWMS';
import OSM from 'ol/source/OSM';
import XYZ from 'ol/source/XYZ';
import Group from 'ol/layer/Group';


export const osm = new TileLayer({
    source: new OSM({
        attributions: '© contributeurs OpenStreetMap'
    }),
    title: 'OpenStreetMap',
    type: 'base',
});

export const otm = new TileLayer({
    source: new XYZ({
        url:'https://{a-c}.tile.opentopomap.org/{z}/{x}/{y}.png',
        attributions: '© contributeurs OpenStreetMap, SRTM | Style cartographique: © OpenTopoMap (CC-BY-SA)'
    }),
    title: 'OpenTopoMap',
    type: 'base',
    
}); 

export const ortho = new TileLayer({
    source: new TileWMS({
        url: 'https://geoservices.wallonie.be/arcgis/services/IMAGERIE/ORTHO_LAST/MapServer/WMSServer',
        params: {'LAYERS': '0', 'TILED': true},
        crossOrigin: 'Anonymous',
        attributions: '© Orthophotoplan SPW'
    }),
    title: 'Orthophotoplan',
    type: 'base',
});

export const ignOverlay = new TileLayer({
    source: new TileWMS({
        url: 'https://wms.ngi.be/cartoweb/1/service?version=1.3.0',
        params: {'LAYERS': 'cartoweb_overlay'},
        projection: 'EPSG:4326',
        crossOrigin: 'Anonymous',
    }),
    //title: 'IGN overlay',
    visible: false
});

const getCurrentBaseLayer = (map) => {
    /**
    *  Returns the current base layer from a map object. Must be unique.
    *  @param {object} map - An OpenLayers map object
    */
    let baseLayers;
    map.getLayers().forEach(g => {
        if (typeof g.getLayers === 'function' && g instanceof Group) {
            g.getLayers().forEach(l => {
                if (l.getVisible() && l.get('type') === 'base') {
                    baseLayers = l;
                }
            });
        } else {
            if (g instanceof VectorLayer || g instanceof TileLayer) {
                if (g.getVisible() && g.get('type') === 'base') {
                    baseLayers = l;
                }
            }
        }
    });
    return baseLayers;
};


export const addIgnOverlayToOrtho = (map) => {    
    map.on('moveend', e => { //TODO better event needed ! when changing the layer only
        if (getCurrentBaseLayer(map).get('title') === 'Orthophotoplan') {
            ignOverlay.setVisible(true);
        } else {
            ignOverlay.setVisible(false);
        }
    })
};
