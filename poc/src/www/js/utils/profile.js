import { LinePlot, AxisBottom } from 'd3plus-plot';

import { itinerarySource } from "../map/layers";
import { openBottomPanel } from "../events/panels";

const SPW_URL_BEGIN = 'http://geoservices.wallonie.be/arcgis/rest/services/GEOTRAITEMENT/Profil/GPServer/Profil/execute?f=json&env:outSR=31370&env:processSR=31370&InputLineFeatures={"fields":[{"name":"OID","type":"esriFieldTypeObjectID","alias":"OID"}],"geometryType":"esriGeometryPolyline","features":[{"geometry":{"paths":[[';
const SPW_URL_END = ']],"spatialReference":{"wkid":3857}},"attributes":{"OID":1}}],"sr":{"wkid":3857}}&ProfileIDField=OID&DEMResolution=1m&MaximumSampleDistance=1&MaximumSampleDistanceUnits=Meters&returnZ=true&returnM=true'


const MAX_PROFILE = 100;
const MAX_COORDINATES = 200;

const processCoordinates = (coordinates) => {
    /**
    *  remove some coordinates when the size of the coordinates array exceeds MAX_COORDINATES
    *  @param [number, number][] coordinates
    */
    let processedCoordinates = [];
    if (coordinates.length > MAX_COORDINATES){
        const averageFactor = Math.round(coordinates.length / MAX_COORDINATES);
        for (let i = 0; i < (coordinates.length / averageFactor) - 1; i++) {
            processedCoordinates.push([
                (coordinates[i*averageFactor][0] + coordinates[(i+1)*averageFactor][0]) / 2,
                (coordinates[i*averageFactor][1] + coordinates[(i+1)*averageFactor][1]) / 2 //TODO make a better average than the average of the first and last item
            ]);
        }
    } else {
        processedCoordinates = coordinates;
    };
    processedCoordinates = processedCoordinates.map(arr => arr.map(x => Math.round(x)))
    return processedCoordinates;
};

const processElevationData = (profileData) => {
    /**
    *  Smooth the elevation data with a moving average window when the size of the profile data exceeds MAX_PROFILE 
    *  @param {id:string, x: number, y: number}[] profileData
    */
    let processedProfile = [];
    if (profileData.length > MAX_PROFILE){
        const averageFactor = Math.round(profileData.length / MAX_PROFILE);
        for (let i = 0; i < (profileData.length / averageFactor) - 1; i++) {
            processedProfile.push({
                id: 'z',
                x: Math.round((profileData[i*averageFactor].x + profileData[(i+1)*averageFactor].x) / 2),
                y: Math.round((profileData[i*averageFactor].y + profileData[(i+1)*averageFactor].y) / 2 *100)/100 //TODO make a better average than the average of the first and last item
            });
        }
    } else {
        for (let i = 0; i < (profileData.length) - 1; i++) {
            processedProfile.push({
                id: 'z',
                x: Math.round(profileData[i].x),
                y: Math.round(profileData[i].y * 100)/100,
            });
        }
    };
    return processedProfile;
};


const fetchSPWProfile = (coordinates) => {
    /**
    *  Fetch an array of coordinates and returns a response of profile elevation along the coordinates tracks
    *  @param [number, number][] coordinates - coordinates in EPSG:3857
    */

    const processedCoordinates = processCoordinates(coordinates);
    const coordinatesStr = `[${processedCoordinates.join('],[')}]`;
    const url = `${SPW_URL_BEGIN}${coordinatesStr}${SPW_URL_END}`;
    fetch(url)
        .then(response => {
            console.log(response);
            return response.json();
        })
        .then(data => {
            // display the profile panel
            openBottomPanel();
            
            const profileXYZL = data.results[0].value.features[0].geometry.paths[0];

            let profileLZ = [];
            for (let i = 0; i < profileXYZL.length; i++) {
                profileLZ.push({id:'z', x: profileXYZL[i][3], y:profileXYZL[i][2] });
            };
            const profileStatistics = computeProfileStatistics(profileLZ);
            updateProfileStatistics(profileStatistics);

            const processedProfile = processElevationData(profileLZ);
            updateProfileGraph(processedProfile);

            // const processedProfileStatistics = computeProfileStatistics(processedProfile);
        });
};


const computeProfileStatistics = (profileData) => {
    /**
    *  Compute profile statistics: return an object with the cumulative ascent and descent, 
    *  the minimal and maximal altitude and length of the profile. All units are meters. 
    *  @param {id:string, x: number, y: number}[] profileData
    */

    let ascent = 0; 
    let descent = 0;
    let maxZ = 0;
    let minZ = 100000;
    for (let i = 0; i < profileData.length - 1; i++) {
        let diff = profileData[i+1].y - profileData[i].y;
        diff > 0 ? ascent = ascent + diff : descent = descent - diff;
        maxZ < profileData[i].y ? maxZ = profileData[i].y : maxZ = maxZ;
        minZ > profileData[i].y ? minZ = profileData[i].y : minZ = minZ;
    }
    const profileLength = profileData[profileData.length-1].x;
    return {
        'ascent': ascent,
        'descent': descent,
        'minZ': minZ,
        'maxZ': maxZ,
        'length': profileLength
    };

}

const updateProfileGraph = (profileData) => {
    const panelContent = document.getElementById('bottomPanelContent');
    const svg = panelContent.querySelectorAll('svg');
    if (svg.length > 0) {
        panelContent.removeChild(svg[0]);
    }

    var dateRange = ["06/30/2015", "08/31/2016"];

    // var bottom = new AxisBottom()
    // .domain(dateRange)
    // .labels(dateRange)
    // .ticks(dateRange)
    // .tickFormat(function(ms) {
    //   return new Date(ms).getFullYear();
    // })
    // .scale("time")
    // .width(600)
    // .height(300)
    // .render();

    const plot = new LinePlot()
        .config({
            data: profileData,
        })
        .height(250)
        .xConfig({
            //tickFormat: m => {m/1000}
            maxSize: 10
        })
        .select("div#bottomPanelContent")
        .render();

        
    return plot;
};

export const updateProfile = () => {
 

    // fetch the profile
    const coordinates = itinerarySource.getFeatures()[0].getGeometry().getCoordinates();
    fetchSPWProfile(coordinates);
};

const updateProfileStatistics = (profileStatistics) => {

    const summary = document.getElementById('routingSummary');

    const oldSummary = document.querySelectorAll('div.profileSummary');
    if (oldSummary.length > 0) {
        oldSummary.forEach(e => e.parentNode.removeChild(e));
    }
    let profileStatisticsDiv = document.createElement('div');
    profileStatisticsDiv.classList.add('profileSummary','container');

    const lengthDiv = document.createElement('div');
    const ascentDiv = document.createElement('div');
    const descentDiv = document.createElement('div');
    const minZDiv = document.createElement('div');
    const maxZDiv = document.createElement('div');

    lengthDiv.classList.add('length','col-sm');
    ascentDiv.classList.add('ascent','col-sm');
    descentDiv.classList.add('descent','col-sm');
    minZDiv.classList.add('minZ','col-sm');
    maxZDiv.classList.add('maxZ','col-sm');

    lengthDiv.innerHTML = `<i class="fas fa-bicycle"></i><span>${Math.round(profileStatistics.length/10)/100} km</span>`;
    ascentDiv.innerHTML = `<i class="fas fa-level-up-alt"></i><span>${Math.round(profileStatistics.ascent)} m</span>`;
    descentDiv.innerHTML = `<i class="fas fa-level-down-alt"></i><span>${Math.round(profileStatistics.descent)} m</span>`;
    minZDiv.innerHTML = `<i class="fas fa-caret-down"></i><span>${Math.round(profileStatistics.minZ*10)/10} m</span>`;
    maxZDiv.innerHTML = `<i class="fas fa-caret-up"></i><span>${Math.round(profileStatistics.maxZ*10)/10} m</span>`;
    
    let row1 = document.createElement('div');
    row1.classList.add('row');
    row1.appendChild(lengthDiv);
    profileStatisticsDiv.appendChild(row1);
    
    let row2 = document.createElement('div');
    row2.classList.add('row');
    row2.appendChild(minZDiv);
    row2.appendChild(maxZDiv);
    profileStatisticsDiv.appendChild(row2);
    
    let row3 = document.createElement('div');
    row3.classList.add('row');
    row3.appendChild(ascentDiv);
    row3.appendChild(descentDiv);
    profileStatisticsDiv.appendChild(row3);

    summary.appendChild(profileStatisticsDiv);
};
