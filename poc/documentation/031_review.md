
## Review des expériences utilisateurs pour tracer un itinéraire {#sec:review}

Sur base d'une review d'applications de randonnées (cfr Annexe 2, page \pageref{sec:annexe2}), nous avons mis en évidence 
les expériences utilisateurs les plus courantes et/ou les plus abouties pour tracer un itinéraire sur une carte interactive. En effet, il est indispensable que notre application ait un comportement similaire à ce que les utilisateurs potentiels peuvent connaître dans d'autres applications.


Les fonctionnalités suivantes ont été ou seront implémentées par ordre de priorité. 

1) Créer l'itinéraire en uploadant un fichier externe (GPX, geojson, ...)
2) Click et déplacé de la souris pour modifier l'itinéraire (ajout de point intermédiaires), le recalcul rapide de l'itinéraire à la volée étant un plus.
3) Click sur la carte pour ajouter point de départ et arrivée
4) Afficher des marqueurs de distances sur le tracé
5) Permettre un calcul selon différent profils (marche, vtt, cyclo, etc.)
6) Possibilité de dessiner un itinéraire à la main (sans passage par des chemins)
7) Possibilité de supprimer des points: en cliquant dessus (komoot, cycle.travel) ou en sélectionnant un outil au préalable (bikemap)
8) Afficher des profils altimétriques
9) Mode "roundtrip" comme sur cycle.travel 
10) Mode "roundtrip" comme sur openrouteservice
11) Afficher des infos de surface (revetement des chemins)

