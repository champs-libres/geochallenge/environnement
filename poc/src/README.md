POC geochallenge environnement
==============================

```bash
docker-compose up
```

The docker-compose builds and runs several containers:

| container name | description        | port  | how to get in touch with...                       |
|----------------|--------------------|-------|---------------------------------------------------|
| db             | postgis databse    | 5496  | psql -h 127.0.0.1 -U postgres -w postgres -p 5496 |
| python         | django application | 18000 | http://localhost:18000                            |
| pgadmin        | pgadmin viewer     | 18001 | http://localhost:18001                            |
| node           | npm application    | 8002  | http://localhost:8002                             |


See the app on the node container only: http://localhost:8002

See the app served by Django (needed to make the intersection work!): http://localhost:18000/webmap. Currently, there is also the need to open a session on the browser while being logged as an administrator to be able to post itineraries.


## Bootstrap

1. build the python container (which contains the required libraries): `docker-compose build python`
2. make the container up: `docker-compose up -d`
3. apply the migrations: `docker-compose exec --user $(id -u) python /app/orgarando/manage.py migrate`

## Import data


```
cat ../data/cantonnements.sql.bz2 | bunzip2 | psql -h 127.0.0.1 -p 5496 -U postgres
```

## QGIS

The directory `./data` contains a QGIS project, and a XML to create a connection to the database.

This allow to visualize events on a map, and data.

## Backend

### Events

Events are an event organize, with (currently) one itinerary.

#### List of events

http://localhost:18000/api/events/

#### See an event

http://localhost:18000/api/events/2/ (where 2 is the event id)

#### See all procedures attached to an event

http://localhost:18000/api/events/2/procedures (where 2 is the event id)

**Procedure** is an administrative task to get authorization to organize the event.

```
[
  {
      "procedureType": "dnf_cantonnement",
      "title": "Traversée du cantonnement DNF de { canton }",
      "description": "Instructions here",
      "intersection3857": {
          # **warning**: this may be a LineString **or** a MultiLineString !
          "type": "MultiLineString",
          "coordinates": [
              [
                  [
                      195919.04175878334,
                      115483.23667987688
                  ],
          # ...
          ]
      },
      "contact": "PUISSANT Thomas, ATTACHE",
      "tel": "082/67.68.90",
      "email": "dinant.cantonnement.dnf.dgarne@spw.wallonie.be",
      # an arbitrary dictionnary of data
      "options": {
          "ingenieur": "PUISSANT Thomas",
          "grade": "ATTACHE",
          "tel": "082/67.68.90",
          "email": "dinant.cantonnement.dnf.dgarne@spw.wallonie.be",
          "canton": "DINANT"
      }
  }
]
```
